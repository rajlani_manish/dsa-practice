package learning;

public class TraverseSubArrays {

	public static void main(String[] args) {
		int A[] = {3, 4, 5};
		//traverseSubArrayBF(A);
		sumAllSubArraysBF(A);
	}
	
	public static void traverseSubArrayBF(int[] A) {
		for (int i = 0; i < A.length; i++) {
			for (int j = i; j < A.length; j++) {
				for (int k = i; k <= j; k++) {
					System.out.print(A[k] + " ");
				}
				System.out.println();
			}
		}
	}
	
	public static void sumAllSubArraysBF(int[] A) {
		int totalSum = 0;
		for (int i = 0; i < A.length; i++) {
			for (int j = i; j < A.length; j++) {
				int sum = 0;
				for (int k = i; k <= j; k++) {
					sum += A[k];
				}
				totalSum+= sum;
				System.out.println(sum);
			}
		}
		System.out.println("totalSum = " + totalSum);
	}

}
