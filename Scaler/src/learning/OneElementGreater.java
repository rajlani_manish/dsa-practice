package learning;

import java.util.Arrays;

public class OneElementGreater {

	// ****Sagar
	public static void main(String[] args) {
		int A[] = { -3, 2, 6, 8, 4, 8, 5 };

		
		int maxCount = 0;
		Arrays.sort(A);
		int max = A[A.length - 1];
		/*
		 * for (int i = 0; i < A.length; i++) { if (A[i] > max) max = A[i]; }
		 */

		for (int i = 0; i < A.length; i++) {
			if (max == A[i])
				maxCount++;
		}

		System.out.println(A.length - maxCount);

	}

}
