package com.learning.practice.array;

import java.util.Arrays;

public class PrefixSum {

	public static void main(String[] args) {
		int[] A = { -3, 6, 2, 4, 5, 2, 8, -9, 3, 1 };
		// output - [-3, 3, 5, 9, 14, 16, 24, 15, 18, 19]
		int[] ps = new int[A.length];
		ps[0] = A[0];
		for (int i = 1; i < A.length; i++) // will start with 1 coz already covered 0 above
		{
			ps[i] = ps[i - 1] + A[i];
		}
		// TC: O(N)
		System.out.println(Arrays.toString(ps));
	}

	// if we have Q queries. Each query has start and end index then print the sum
	// of subarray for every query. Brute force takes Q*N.. but if we do with this
	/*
	 * // above approach it will take TC: Q+N // 1. build PS // 2. run Q times loop
	 * pass start and end calculate the sum = sum(s,e)= ps[e]-p[s-1] //it will be
	 * O(1) so Q *1 times + N(for preparing PS)
	 */

}

/*
 * Day 26 - Array - Prefix sum and carry forward
 * 
 * https://scaler-production-new.s3.ap-southeast-1.amazonaws.com/attachments/
 * attachments/000/003/478/original/Prefix_Sum.pdf?X-Amz-Algorithm=AWS4-HMAC-
 * SHA256&X-Amz-Credential=AKIAIDNNIRGHAQUQRWYA%2F20211112%2Fap-southeast-1%2Fs3
 * %2Faws4_request&X-Amz-Date=20211112T062458Z&X-Amz-Expires=561600&X-Amz-
 * SignedHeaders=host&X-Amz-Signature=
 * db29b0a2a3b7ca96df3983d7064a9003875e91af9a137bcca6a56f8bfe222ee1
 */
