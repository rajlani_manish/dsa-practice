/*
 * package com.example.mockinterview;
 * 
 * public class ThrowBall {
 * 
 * public static void main(String[] args) { // TODO Auto-generated method stub
 * 
 * }
 * 
 * public class Solution { public int[] solve(int[] A, int[] B) { } }
 * 
 * 
 * }
 * 
 * 
 * You are playing a game called “Ball Throw”. Each throw has value of either 2
 * or 3 points.
 * 
 * 1. A throw is worth 2 points if the throw distance is less than or equal to
 * "D" meters.
 * 
 * 2. A throw is worth 3 points if the throw distance is larger than "D" meters,
 * where "D" is some non-negative integer. There are 2 teams competing and you
 * need to get the maximum advantage for your team i.e (the points of the first
 * team minus the points of the second team) has to be maximum.
 * 
 * Choose some value of D that would do the task and tell the the scores for
 * both team for that particular value of D.
 * 
 * Input
 * 
 * The first argument is an array of length N (1 ≤ N ≤ 200000) - the number of
 * throws of the first team. It contains array of N integer numbers — the
 * distances of throws Ai (1 ≤ Ai ≤ 2000000000).
 * 
 * The second argument is an array of length M (1 ≤ M ≤ 200000) - the number of
 * throws of the second team. It contains array of M integer numbers — the
 * distances of throws Bi (1 ≤ Bi ≤ 2000000000). Output
 * 
 * Return array of two elements "A" "B" where "A" is first team's score and "B"
 * is second teams score with an optimal "D". If there are several values of "D"
 * which optimize "A-B", find the one in which "A" is maximum. Examples
 * 
 * Input
 * 
 * 1 2 3 5 6 Output
 * 
 * 9 6 Example Explanation : We can keep D = 0. Team A will then score 3 points
 * on each of their throws with final score as 9. Team B scores 6. The
 * difference is then 3 which is the most optimal difference possible.
 * 
 * Input
 * 
 * 6 7 8 9 10 1 2 3 4 5 Output
 * 
 * 15 10
 */