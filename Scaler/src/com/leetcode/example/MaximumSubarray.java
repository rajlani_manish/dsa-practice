package com.leetcode.example;

public class MaximumSubarray {

	public static void main(String[] args) {
		System.out.println(maxSubArray(new int[] {-2,1,-3,4,-1,2,1,-5,4}));
		System.out.println(maxSubArray(new int[] {-2, -3, 4, -1, -2, 1, 5, -3}));
	}

	public static int maxSubArray(int[] nums) {
		int maxSoFar = Integer.MIN_VALUE;
		int maxEndingHere = 0;

		for (int i = 0; i < nums.length; i++) {
			maxEndingHere += nums[i];
			maxSoFar = Math.max(maxSoFar, maxEndingHere);
			if (maxEndingHere <= 0)
				maxEndingHere = 0;
		}
		return maxSoFar;
	}
}


/*
 * 53. Maximum Subarray Easy
 * 
 * 17109
 * 
 * 811
 * 
 * Add to List
 * 
 * Share Given an integer array nums, find the contiguous subarray (containing
 * at least one number) which has the largest sum and return its sum.
 * 
 * A subarray is a contiguous part of an array.
 * 
 * 
 * 
 * Example 1:
 * 
 * Input: nums = [-2,1,-3,4,-1,2,1,-5,4] Output: 6 Explanation: [4,-1,2,1] has
 * the largest sum = 6. Example 2:
 * 
 * Input: nums = [1] Output: 1 Example 3:
 * 
 * Input: nums = [5,4,-1,7,8] Output: 23
 * 
 * 
 * Constraints:
 * 
 * 1 <= nums.length <= 105 -104 <= nums[i] <= 104
 */