package com.beginner.scaler.example.arrays;

import java.util.ArrayList;

public class AddOneToNumber {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(1);
		list.add(1);
		list.add(3);
		list.add(2);
		list.add(1);
		list.add(1);
		list.add(2);
		list.add(5);
		list.add(9);
		list.add(6);
		list.add(5);
		// list.add(3);
		// 1, 1, 1, 3, 2, 1, 1, 2, 5, 9, 6, 5
		System.out.println(plusOne(list));
	}

	public static ArrayList<Integer> plusOne(ArrayList<Integer> A) { // 999 - 1000
		ArrayList<Integer> outPut = new ArrayList<>();
		int sum = 0, carry = 0;
		// 123 + 1
		// 999+1
		String strNumber = "";

		for (Integer element : A) {
			strNumber += element;
		}
		String result = "";
		int a = strNumber.length() - 1;
		sum = strNumber.charAt(a) - '0' + 1;
		while (a >= 0) {
			sum += carry;
			result = Integer.toString(sum %10) + result;
			carry = sum / 10;
			a--;
		}
		if (carry == 1)
			result = carry + result;

		outPut.add(Integer.parseInt(result));

		return outPut;

	}

}
