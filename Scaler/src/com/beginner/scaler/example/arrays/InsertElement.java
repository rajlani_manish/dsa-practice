package com.beginner.scaler.example.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class InsertElement {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Array size: ");
		int size = sc.nextInt();

		int[] array = new int[size + 1];
		for (int i = 0; i < size; i++) {
			array[i] = sc.nextInt();
		}
		System.out.print(Arrays.toString(array));

		System.out.println("Insertion Position ");
		int position = sc.nextInt();

		System.out.println("Insertion Element ");
		int newElement = sc.nextInt();

		for (int i = size; i >= position; i--) {
			array[i] = array[i - 1];
		}
		array[position - 1] = newElement;

		for (int i = 0; i <= size; i++) {
			System.out.print(array[i]);
		}

		sc.close();

	}

}
