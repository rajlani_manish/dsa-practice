package com.beginner.scaler.example.arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SearchElement {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter testCases: ");
		int testCases = sc.nextInt();

		while (testCases > 0) {
			searchElement(sc);
			testCases--;
		}

		sc.close();

	}

	private static void searchElement(Scanner sc) {
		List<Integer> elements = new ArrayList<>();

		System.out.print("Enter Size of array: ");
		int size = sc.nextInt();

		if (size > 0) {
			System.out.print("Enter elements of array: ");
			for (int i = 0; i < size; i++) {
				elements.add(sc.nextInt());
			}
		}

		System.out.print("Enter element to search in this array: ");
		if (!elements.isEmpty()) {
			int searchElement = sc.nextInt();

			System.out.print(elements.contains(searchElement) ? 1 : 0);
		}
	}

}
