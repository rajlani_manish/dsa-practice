package com.beginner.scaler.example.arrays;

import java.util.Scanner;

public class FirstVSLast {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		while (testCases > 0) {
			int numberToBeChecked = sc.nextInt();
			int lastDigit = 0;
			
			lastDigit = numberToBeChecked % 10;
			int firstDigit = numberToBeChecked;
			while (firstDigit >= 10) {
				firstDigit = firstDigit / 10;
			}
			System.out.println("LastDigit :" + lastDigit);
			System.out.println("FirstDigit :" + firstDigit);
			testCases--;
		}
		sc.close();
	}
}

/*
 * First vs Last Problem Description
 * 
 * Write a program to input T numbers(N) from user and print first and last
 * digits of the given numbers.
 * 
 * Problem Constraints 1 <= T <= 1000
 * 
 * 0 <= N <= 100000000
 * 
 * Input Format First line is T which means number of test cases.
 * 
 * Each next N lines contain an integer N.
 * 
 * Output Format T lines each containing two space separated integers
 * representing first and last digits of the input integer.
 * 
 * Example Input Input 1:
 * 
 * 2 5 1001 Input 2:
 * 
 * 2 10023 1589
 * 
 * Example Output Output 1:
 * 
 * 5 5 1 1 Output 2:
 * 
 * 1 3 1 9
 * 
 * Example Explanation Explanation 1:
 * 
 * 5 has fist and last digits same. 1001 also has fist and last digits same.
 * Explanation 2:
 * 
 * 10023 has 1 as first digit and 3 as last digit. 1589 has 1 as first digit and
 * 9 as last digit.
 */
