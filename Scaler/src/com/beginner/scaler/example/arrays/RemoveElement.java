package com.beginner.scaler.example.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class RemoveElement {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size: ");
		int size = sc.nextInt();

		int[] array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = sc.nextInt();
		}
		System.out.print(Arrays.toString(array));

		System.out.println("Enter position to be removed: ");
		int position = sc.nextInt();

		int[] newArray = new int[size - 1];
		for (int i = 0, k = 0; i < array.length; i++) {
			if (i == position - 1) {
				continue;
			}
			newArray[k++] = array[i];
		}

		for (int i = 0; i < newArray.length; i++) {
			System.out.print(newArray[i] + " ");
		}
		sc.close();
	}
}

/*
 * Remove that Problem Description
 * 
 * Write a program to input N numbers array from user and delete an element from
 * it at specified position X.
 * 
 * Problem Constraints
 * 
 * 1 <= N <= 100
 * 
 * 1 <= A[i] <= 1000
 * 
 * 1 <= X <= N
 * 
 * 
 * 
 * Input Format First line is N which means number of elements.
 * 
 * Second line contains N space separated integers.
 * 
 * Third line is X position which has to be deleted.
 * 
 * 
 * 
 * Output Format N-1 space separated integers of the input array after deleting
 * the element at required position.
 * 
 * 
 * 
 * Example Input Input 1:
 * 
 * 5 2 3 1 4 2 3
 * 
 * 
 * Example Output Output 1:
 * 
 * 2 3 4 2
 * 
 * 
 * Example Explanation Explanation 1:
 * 
 * Clearly after removing A[3] = 1, remaining array is 2 3 4 2.
 */