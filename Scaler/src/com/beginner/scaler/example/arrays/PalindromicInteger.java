package com.beginner.scaler.example.arrays;

import java.util.Scanner;

public class PalindromicInteger {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number: ");
		StringBuilder input = new StringBuilder(sc.nextLine());
		StringBuilder reverseString = new StringBuilder(input);
		//System.out.println(input.reverse());
		String reverse = input.reverse().toString();
		if (reverseString.toString().equals(reverse)) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}

		sc.close();
	}
	
	/*
	 * private static String checkIntPalindromic() {
	 * 
	 * }
	 */

}
