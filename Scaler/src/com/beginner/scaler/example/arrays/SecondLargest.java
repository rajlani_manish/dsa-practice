package com.beginner.scaler.example.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class SecondLargest {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();

		int[] arr = new int[size];

		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}

		Arrays.sort(arr);

		if (size > 1 && arr[arr.length-2] != arr[arr.length-1])
			System.out.println(arr[arr.length-2]);
		else
			System.out.println("NA");

		sc.close();

	}

}
