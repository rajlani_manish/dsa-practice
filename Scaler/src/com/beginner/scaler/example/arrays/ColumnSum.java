package com.beginner.scaler.example.arrays;

import java.util.Arrays;

public class ColumnSum {

	public static void main(String[] args) {
		int[][] A = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 2, 3, 4 } };
		// 0,0 1,0 2,0
		System.out.println(Arrays.toString(solve(A)));
		System.out.println(Arrays.toString(solvee(A)));
	}

	public static int[] solve(int[][] A) {
		int[] columnSum = new int[A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				columnSum[j] += A[i][j];
				System.out.print(columnSum[j] + " ");
			}
		}
		return columnSum;
	}
	
	public static int[] solvee(int[][] A) {
		int[] columnSum = new int[A[0].length];
		for (int i = 0; i < A[0].length; i++) {
			for (int j = 0; j < A.length; j++) {
				columnSum[i] += A[j][i];
				System.out.print(columnSum[j] + " ");
			}
		}
		return columnSum;
	}

}

/*
 * Column Sum Problem Description
 * 
 * You are given a 2D integer matrix A, return a 1D integer vector containing
 * column-wise sums of original matrix.
 * 
 * Problem Constraints 1 <= A.size() <= 103
 * 
 * 1 <= A[i].size() <= 103
 * 
 * 1 <= A[i][j] <= 103
 * 
 * Input Format First argument is a vector of vector of integers.(2D matrix).
 * 
 * 
 * Output Format Return a vector conatining column-wise sums of original matrix.
 * 
 * 
 * Example Input Input 1:
 * 
 * [1,2,3,4] [5,6,7,8] [9,2,3,4]
 * 
 * 
 * Example Output Output 1:
 * 
 * {15,10,13,16}
 * 
 * 
 * Example Explanation Explanation 1
 * 
 * Column 1 = 1+5+9 = 15 Column 2 = 2+6+2 = 10 Column 3 = 3+7+3 = 13 Column 4 =
 * 4+8+4 = 16
 */

/* fact of number */

/*
 * int Solution::solve(int A, int B, int C, int D) { long long int a = A, b = B,
 * c = C, d = D; d -= 1; long long int ans = (d / a + d / b + d / c); ans -= (d
 * / ((a*b)/__gcd(a, b)) + d / ((c*b)/__gcd(c, b)) + d / ((a*c)/__gcd(a, c)));
 * long long int temp = ((a*b)/__gcd(a, b)); if (temp <= d) ans += (d / ((temp *
 * c) / __gcd(temp, c))); return ans; }
 */