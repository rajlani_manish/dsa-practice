package com.beginner.scaler.example.arrays;

import java.util.Scanner;

public class NegativeIntegers {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();

		if (size >= 1 && size <= 1000) {
			int[] A = new int[size];
			for (int i = 0; i < A.length; i++) {
				A[i] = sc.nextInt();
			}

			for (int i = 0; i < A.length; i++) {
				if (A[i] < 0) {
					System.out.print(A[i] + " ");
				}
			}
		}
		sc.close();
	}

}

/*
 * Problem Description
 * 
 * Write a program to print all negative numbers from input array A of size N
 * where you have to take integer N and further N elements as input from user.
 * 
 * Problem Constraints 1 <= N <= 1000
 * 
 * -1000 <= A <= 1000
 * 
 * Input Format
 * 
 * A single line representing N followed by N integers of the array A
 * 
 * Output Format A single line containing elements from the input array which
 * are negative in the same order.
 * 
 * Example Input Input 1:
 * 
 * 5 1 -5 2 -8 -4 Input 2:
 * 
 * 4 -1 0 -8 -1
 * 
 * Example Output Output 1:
 * 
 * -5 -8 -4 Output 2:
 * 
 * -1 -8 -1
 */
