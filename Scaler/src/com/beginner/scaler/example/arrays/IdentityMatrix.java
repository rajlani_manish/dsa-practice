package com.beginner.scaler.example.arrays;

public class IdentityMatrix {

	public static void main(String[] args) {
		int A[][] = {{1, 0}, {0, 1}};
		System.out.println(solve(A));
	}
	
	public static int solve(final int[][] A) {		
		for (int i = 0; i < A.length; i++) {
			if (A[i][i] != 1)
				return 0;
		}		
		return 1;
    }

}
