
package com.beginner.scaler.example.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class CountFrequency {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size: ");
		int size = sc.nextInt();

		int[] array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = sc.nextInt();
		}

		System.out.println(Arrays.toString(array));

		int[] newArray = new int[size];
		
		for (int i = 0; i < size; i++) {
			int count = 0;
			for (int j = 0; j < size; j++) {
				if (array[i] == array[j]) {
					count = count + 1;					
				}
				newArray[i] = count;
			}

		}

		System.out.println(Arrays.toString(newArray));

		sc.close();

	}

	public ArrayList<Integer> solve(ArrayList<Integer> A) {
		return A;
	}

}

/*
 * Frequency count Problem Description
 * 
 * You are given an integer vector A, you have to return a vector of same size
 * whose ith element is the frequency count of A[i] in vector A .
 * 
 * 
 * 
 * Problem Constraints 1 <= A.size() <= 1000
 * 
 * 1 <= A[i] <= 100
 * 
 * 
 * 
 * Input Format First argument is vector of integers representing array A.
 */

/*
 * Output Format You have to return a vector of integers as per the question.
 * 
 * 
 * 
 * Example Input Input 1:
 * 
 * A = [1, 2, 5, 1, 5, 1 ]
 * 
 * 
 * Example Output Output 1:
 * 
 * [3, 1, 2, 3, 2, 3]
 * 
 * 
 * Example Explanation Explanation 1:
 * 
 * Clearly, In the given array we have 1 three times, 2 one time and 5 two
 * times.
 */