package com.beginner.scaler.example.arrays;

import java.util.ArrayList;

public class PascalTriangle {

	public static void main(String[] args) {
		System.out.println(solve(5));
	}

	public static ArrayList<ArrayList<Integer>> solve(int A) {
		
//		if(A >=1 && A<=25)
		//{
			ArrayList<ArrayList<Integer>> output = new ArrayList<>();
			for(int i = 0; i <A; i++)
			{
				ArrayList<Integer> list = new ArrayList<>();
				for(int j = 0; j <A; j++)
				{
					if(i == 0)
					{
						if(j==0)
						{
							list.add(j,1);
						} else {
							list.add(j, 0);
						}
					} else {
						if(j==0)
						{
							list.add(j, 1);
						} else {
							int no1= output.get(i-1).get(j);
							int no2= output.get(i-1).get(j-1);
							list.add(j, no1+no2);
							
						}
					}
					output.add(i, list);
				}
			}
			
			return output;
		
		//return (ArrayList)Collections.emptyList();
	}

}

/*
 * Q2. Pascal Triangle Unsolved character backgroundcharacter Stuck somewhere?
 * Ask for help from a TA & get it resolved Get help from TA Problem Description
 * 
 * Write a program to input an integer N from user and print pascal triangle up
 * to N rows.
 * 
 * 
 * 
 * Problem Constraints 1 <= N <= 25
 * 
 * 
 * 
 * Input Format First line is an integer N.
 * 
 * 
 * 
 * Output Format N lines whose each row contains N+1 space separated integers.
 * 
 * 
 * 
 * Example Input Input 1:
 * 
 * 3 Input 2:
 * 
 * 5
 * 
 * 
 * Example Output
Output 1:

1 0 0 
1 1 0 
1 2 1 
Output 2:

1 0 0 0 0
1 1 0 0 0
1 2 1 0 0
1 3 3 1 0
1 4 6 4 1 


Example Explanation
Explanation 1:

Row 1 = 1 0 0 0 0
Row 2 = 1C0 1C1 0 0 0= 1 1 0 0 0
Row 3 = 2C0 2C1 2C2 0 0= 1 2 1 0 0
Row 4 = 3C0 3C1 3C2 3C3 0= 1 3 3 1 0
 */
