/*You are given a constant array A and an integer B.

You are required to return another array where outArr[i] = A[i] + B.*/

package com.beginner.scaler.example.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class CopyArray {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int size = sc.nextInt();
		if (1 <= size && size <= 10000) {
			int[] array = new int[size];
			int[] outArray = new int[size];

			for (int i = 0; i < size; i++) {
				array[i] = sc.nextInt();
			}

			int number = sc.nextInt();
			for (int i = 0; i < size; i++) {
				if (1 <= array[i] && array[i] <= 10000) {
					outArray[i] = array[i] + number;
				}
			}
			System.out.print(Arrays.toString(outArray));

		}

		sc.close();
	}

	public int[] solve(final int[] A, int B) {

		if (1 <= A.length && A.length <= 10000) {

			int[] outArray = new int[A.length];
			for (int i = 0; i < A.length; i++) {
				if (1 <= A[i] && A[i] <= 10000) {
					outArray[i] = A[i] + B;
				}
			}
			return outArray;
		}
		return null;
	}

}
