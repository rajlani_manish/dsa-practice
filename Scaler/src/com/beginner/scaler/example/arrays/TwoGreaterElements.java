package com.beginner.scaler.example.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class TwoGreaterElements {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(2);
		list.add(3);
		list.add(1);
		/*
		 * System.out.println(solve1(list)); for (Integer a : list) {
		 * System.out.println(a); }
		 */ // 1, 2, 2, 3, 1
		/*
		 * System.out.println(Arrays.toString(solve2(new int[] { 11, 17, 100, 5 })));
		 * for (Integer a : list) { System.out.println(a); }
		 */

		int[] A = new int[] { 5, 17, 100, 11 };
		System.out.println(Arrays.toString(solve2(A)));

	}

	public static ArrayList<Integer> solve1(ArrayList<Integer> list) {
		Integer maxElement = Integer.MIN_VALUE;
		Integer nextToMax = Integer.MIN_VALUE;
		for (int element : list) {
			if (element > maxElement) {
				nextToMax = maxElement;
				maxElement = element;
			} else if (element > nextToMax) {
				nextToMax = element;
			}
		}
		list.remove(nextToMax);
		list.remove(maxElement);
		return list;
	}

	public static int[] solve2(int[] A) {
		int maxNumber = Integer.MIN_VALUE;
		int nextToMax = Integer.MIN_VALUE;
		int[] newArr = new int[A.length-2];
		for (int i = 0; i < A.length; i++) {
			if (A[i] > maxNumber) {
				nextToMax = maxNumber;
				maxNumber = A[i];
			} else if (A[i] > nextToMax) {
				nextToMax = A[i];
			}
		}

        int idx = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i] < maxNumber && A[i] < nextToMax)
				newArr[idx++] = A[i];
		}

		return newArr;
	}

}

/*
 * Elements which have at-least two greater elements Problem Description
 * 
 * You are given an array of distinct integers A, you have to find and return
 * all elements in array which have at-least two greater elements than
 * themselves.
 * 
 * NOTE: The result should have the order in which they are present in the
 * original array.* *
 * 
 * Problem Constraints 3 <= |A| <= 105
 * 
 * -109 <= A[i] <= 109* * Input Format First and only argument is an integer
 * array A. *
 * 
 * * Output Format Return an integer array containing the elements of A which
 * have at-least two greater elements than themselves in A.
 * 
 * * Example Input Input 1:
 * 
 * A = [1, 2, 3, 4, 5] Input 2:
 * 
 * A = [11, 17, 100, 5]
 * 
 * * Example Output Output 1:
 * 
 * [1, 2, 3] Output 2:
 * 
 * [11, 5] * Example Explanation Explanation 1:
 * 
 * Number of elements greater than 1: 4 Number of elements greater than 2: 3
 * Number of elements greater than 3: 2 Number of elements greater than 4: 1
 * Number of elements greater than 5: 0 Elements 1, 2 and 3 have atleast 2
 * elements strictly greater than themselves. Explanation 2:
 * 
 * Number of elements greater than 11: 2 Number of elements greater than 17: 1
 * Number of elements greater than 100: 0 Number of elements greater than 5: 3
 * Elements 5 and 11 have atleast 2 elements strictly greater than themselves.
 * 
 */
