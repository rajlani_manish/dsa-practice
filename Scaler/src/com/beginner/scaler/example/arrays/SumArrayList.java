package com.beginner.scaler.example.arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class SumArrayList {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int rows, cols;
		rows = sc.nextInt();
		cols = sc.nextInt();

		ArrayList<ArrayList<Integer>> A = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> B = new ArrayList<ArrayList<Integer>>();

		// INput
		System.out.print("ENter 1st array: ");
		for (int i = 0; i < rows; i++) {
			// cols
			ArrayList<Integer> listA = new ArrayList<Integer>();
			A.add(listA);
			for (int j = 0; j < cols; j++) {
				listA.add(sc.nextInt());
			}
		}

		// INput
		System.out.print("ENter 2nd array: ");
		for (int i = 0; i < rows; i++) {
			// cols
			ArrayList<Integer> listB = new ArrayList<Integer>();
			B.add(listB);
			for (int j = 0; j < cols; j++) {
				listB.add(sc.nextInt());
			}
		}

		/*
		 * Print inputs 
		 * for (int i = 0; i < rows; i++) { // cols for (int j = 0; j <
		 * cols; j++) { System.out.print(A.get(i).get(j) + ","); } }
		 */

		System.out.print(solve(A, B));
		sc.close();
	}

	public static ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> A,
			ArrayList<ArrayList<Integer>> B) {
		ArrayList<ArrayList<Integer>> sumList = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < A.size(); i++) {
			ArrayList<Integer> subList = new ArrayList<Integer>();
			sumList.add(subList);
			for (int j = 0; j < A.get(i).size(); j++) {
				subList.add(A.get(i).get(j) + B.get(i).get(j));
			}
		}
		return sumList;
	}
}
