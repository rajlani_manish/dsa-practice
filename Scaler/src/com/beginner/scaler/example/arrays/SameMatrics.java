/*Are Matrices Same ?
Problem Description

You are given two two matrices A & B of equal sizes and you have to check whether two matrices are equal or not.

NOTE: Both matrices are equal if A[i][j] == B[i][j] for all i and j in the given range.

Problem Constraints
1 <= A.size(), B.size() <= 1000

1 <= A[i].size(), B[i].size() <= 1000

1 <= A[i][j], B[i][j] <= 1000

Input Format
First argument is vecotor of vector of integers representing matrix A.*/

/*Second argument is vecotor of vector of integers representing matrix B.

Output Format
Return 1 if both matrices are equal or return 0.

Example Input
Input 1:

A = [[1, 2, 3],
	[4, 5, 6],
	[7, 8, 9]]
B = [[1, 2, 3],
	[4, 5, 6],
	[7, 8, 9]]
	
Input 2:

A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[7, 8, 9],[4, 5, 6]]


Example Output
Output 1:
1
Output 2:
0
Example Explanation
Explanation 1:
==> Clealry all the elements of both matrices are equal at respective postions.
Explanation 2:
==> Clealry all the elements of both matrices are not equal at respective postions.*/

package com.beginner.scaler.example.arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class SameMatrics {

	public static void main(String[] args) {
		int rows, cols;
		Scanner sc = new Scanner(System.in);

		rows = sc.nextInt();
		cols = sc.nextInt();

		int[][] A = new int[rows][cols];
		int[][] B = new int[rows][cols];

		// INput
		System.out.print("ENter 1st array: ");
		for (int i = 0; i < rows; i++) {
			// cols
			for (int j = 0; j < cols; j++) {
				A[i][j] = sc.nextInt();
			}
		}

		System.out.print("ENter 2nd array: ");
		for (int i = 0; i < rows; i++) {
			// cols
			for (int j = 0; j < cols; j++) {
				B[i][j] = sc.nextInt();
			}
		}

		System.out.print(solve(A, B));
		sc.close();
	}

	public static int solve(int[][] A, int[][] B) {
		boolean sameMatrix = true;
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++)
				if (A[i][j] != B[i][j]) {
					sameMatrix = false;
					break;
				}
				
		}
		return sameMatrix ? 1: 0;
	}
	
	public int solve(ArrayList<ArrayList<Integer>> A, ArrayList<ArrayList<Integer>> B) {
		boolean sameMatrix = true;
		for (int i = 0; i < A.size(); i++) {
			for (int j = 0; j < A.get(i).size(); j++)
				if (A.get(i).get(j) != B.get(i).get(j)) {
					sameMatrix = false;
					break;
				}
				
		}
		return sameMatrix ? 1: 0;
    }

}
