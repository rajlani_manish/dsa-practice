package com.beginner.scaler.example.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EvenSum {

	public static void main(String[] args) {
		//Integer[] A = { 12, 23, 24 };
		//System.out.println(solve(Arrays.asList(A)));
		//https://stackoverflow.com/questions/16070070/
		//why-does-list-removeint-throw-java-lang-unsupportedoperationexception/16070111
		ArrayList<Integer> list = new ArrayList<>();
		list.add(12);
		list.add(13);
		list.add(24);
		System.out.println(solve(list));
	}

	public static int solve(List<Integer> A) {
		int maxEvenSum = 0;
		if (A.size() >= 1 && A.size() < 105) {
			for (int i = 0; i < A.size(); i++) {
				maxEvenSum += A.get(i); // Sum up all element
			}

			if (maxEvenSum % 2 == 0) {
				return maxEvenSum; // see if sum is even
			} else { // else remove the min odd element from the list
				Collections.sort(A);
				for (int j = 0; j < A.size(); j++) {
					if (A.get(j) % 2 != 0 && A.get(j) != 0) {
						A.remove(j);
						maxEvenSum = 0;
						break;
					}
				}

				for (int i = 0; i < A.size(); i++) {
					maxEvenSum += A.get(i); // After removing min odd number ..Sum up all element again
				}

			}
		}
		return maxEvenSum;
	}
	
	void solve(){
	    int i = 1;
	    while(i < 4){
	        int x = i;
	        while(x-- > 0){
	            // O(1) operation
	        }
	        i++;
	    }
	}
}

/*
 * Even Sum Problem Description
 * 
 * You are given an array A, having N integers. You have to find the maximum
 * even sum using the elements of Array A only once. i.e you can't use one
 * element twice.
 * 
 * Problem Constraints 1 <= N <= 105
 * 
 * 0 <= A[i] <= 104
 * 
 * Input Format The only argument given is A, an Integer Array.
 * 
 * Output Format Return the maximum even sum you can get using the elements of
 * Array A only once.
 * 
 * Example Input A = [2, 3, 4]
 * 
 * Example Output 6
 * 
 * Example Explanation You can get 6 by using 2 and 4.
 */
