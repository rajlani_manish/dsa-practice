package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Number : ");
		int iterations = sc.nextInt();
		

		for(int i = 0 ; i < iterations ; i++) {
			int number = sc.nextInt();
			int ans = 0;

			while (number > 0) {
				int lastDigit = number % 10;
				ans = ans * 10 + lastDigit;
				number = number / 10;
			}

			System.out.println(ans);
		}
		
		sc.close();
	}

}
