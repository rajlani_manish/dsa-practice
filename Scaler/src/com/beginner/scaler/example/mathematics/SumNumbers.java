package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class SumNumbers {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();

		if (num1 >= 1 && num1 <= Math.pow(10, 7) && num2 >= 1 && num1 <= Math.pow(10, 7)) {
			System.out.print(sum(num1, num2));
		}
		sc.close();
	}
	
	private static int sum(int a, int b)
	{
		return a+b;
	}

}
