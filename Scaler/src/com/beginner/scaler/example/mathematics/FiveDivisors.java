package com.beginner.scaler.example.mathematics;

import java.util.ArrayList;
import java.util.List;

/*Find the count of numbers in the range 1 to 
20000 (both inclusive) which have exactly 5 divisors?*/
/*16
81
625
2401
14641
28561*/

public class FiveDivisors {

	public static void main(String[] args) {
		int exactly5DivisorsCount = 0;
		for (int i = 1; i <= 20000; i++) {
			int count = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0)
					count++;
			}
			if (count == 3) {
				//System.out.println(i);
				exactly5DivisorsCount++;
			}
		}
		//System.out.println("total count having n divisors : " + exactly5DivisorsCount);
		optimalApproach();
	}

	public static void optimalApproach() {
		// 20000 - N ^ 4
		// 20000 - 2^4, 3^4, 5^4, 7^4, 11^4
		// for 50000 - 2^4, 3^4, 5^4, 7^4, 11^4, 13^4
		int exactly5DivisorsCount = 0;
		List<Double> list = new ArrayList<>();
		for (double i = 2; i * i * i * i <= 20000; i++) {
			System.out.println(i);
			list.add(i * i * i * i);
		}
		System.out.println(list);

		for (Double element : list) {
			int count = 0;
			for (int j = 1; j <= element; j++) {
				if (element % j == 0)
					count++;
			}
			if (count == 5) {
				System.out.print(element+ ", ");
				exactly5DivisorsCount++;
			}
			
		}
		System.out.println();
		System.out.println("total count having 5 divisors : " + exactly5DivisorsCount);

	}

}
