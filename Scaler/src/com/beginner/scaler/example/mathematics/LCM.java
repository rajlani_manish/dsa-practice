package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class LCM {

	public static void main(String a[])
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Numbers : ");
		int number1 = sc.nextInt();
		int number2 = sc.nextInt();
		int gcd = 1;
		for(int i=1; i<= Math.min(number2, number1); i++)
		{
			if(number1%i==0 && number2%i==0)
				gcd = i;
				
		}
		System.out.print(number1*number2/gcd);	
		sc.close();
	}
}
