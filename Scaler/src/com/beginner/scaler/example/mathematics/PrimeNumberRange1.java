package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

class PrimeNumberRange1 {
	public static void main(String arg[]) {
		System.out.print("Enter n value : ");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println("Prime numbers between 1 to " + n + " are ");
		for (int start = 2; start <= n; start++) {
			int count = 0;
			for (int i = 2; i <= start; i++) {
				if (start % i == 0) {
					count++;
				}
			}
			if (count == 1)
				System.out.print(start + "  ");
		}
		sc.close();
	}
	
	public static void printPrimes(int number) {
		
	}
}
