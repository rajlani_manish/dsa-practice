package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class PowerFunction {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();

		if (num1 >= 1 && num2 <= 1000) {
			System.out.print(getPower(num1, num2));
		}
		sc.close();

	}

	private static int getPower(int num1, int num2) {
		int output = 1;
		for (int i = 1; i <= num2; i++) {
			output = output * num1;
		}
		return output;
	}

}
