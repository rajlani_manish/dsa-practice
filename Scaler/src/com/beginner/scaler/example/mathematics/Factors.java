package com.beginner.scaler.example.mathematics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Factors {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int iterations = sc.nextInt();

		if (1 <= iterations && iterations <= 1000) {
			while (iterations > 0) {
				int number = sc.nextInt();
				int i;
				List<Integer> list = new ArrayList<>();
				for ( i= 1; i <= Math.sqrt(number); i++) {
					if (number % i == 0) {
						/*System.out.print(i + " ");
						System.out.print(number / i + " ");*/
						list.add(i);
						if(number/i !=  Math.sqrt(number) )
						list.add(number / i);
					}
				}

				Collections.sort(list);
				for(Integer factors: list) {
					System.out.print(factors + " ");
				}
				
				/*if (number % i == i) {
					System.out.print(i + " ");
				}*/
				
				System.out.println();
				iterations--;
			}
		}
		
		/* more optimized approach
		 * for(int i=1; i <= Math.sqrt(number); i++) {
	         if(number % i == 0) {
	             System.out.print(i+"\t");

	             if(number/i != Math.sqrt(number) )
	             System.out.print((number/i)+"\t");
	          }
	       }*/

		sc.close();
	}

}

/*
 import java.lang.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
		int iterations = sc.nextInt(); // 100

		if (1 <= iterations && iterations <= 1000) {
			while (iterations > 0) {
				int number = sc.nextInt();
				int i;
				for ( i= 1; i < Math.sqrt(number); i++) {
					if (number % i == 0) {
						System.out.print(i + " ");
						System.out.print(number / i + " ");
					}
				}
				if (number % i == 0) {
					System.out.print(i + " ");
				}
				System.out.println();
				iterations--;
			}
		}
		sc.close();
        
    }
}
 * */
