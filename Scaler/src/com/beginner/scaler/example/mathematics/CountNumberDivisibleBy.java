package com.beginner.scaler.example.mathematics;

/*Find the count of numbers in the range 1 to 500 
which are either divisible by 3 or 7 or 9?*/
public class CountNumberDivisibleBy {

	public static void main(String[] args) {
		int count = 0;
		for (int i = 1; i <= 500; i++) {
			if (i % 3 == 0 || i % 7 == 0 || i % 9 == 0)
				count++;
		}
		System.out.println(count);
	}

}
