package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class PrimeNumbersRange2 {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number"); // 868725
		int number = sc.nextInt();

		sc.close();
		int start = 1;
		while (start <= number) { // 2<= 5
			boolean isPrime = false; // true
			for (int num = start; num >= 1; num--) {
				if (start % num == 0) {
					isPrime = true;
				}
			}
			if (isPrime) {
				System.out.print("Prime :" + start);				
			}
			start++;

		}

	}

}

/////