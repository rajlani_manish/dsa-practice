package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

/* Name of the class has to be "Main" only if the class is public. */
class PrimeNumbers {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number"); // 868725
		int number = sc.nextInt();
		System.out.println(checkPrime(number));
		sc.close();
	}

	static String checkPrime(int number) {
		int i = 2;
		for (; i <= Math.sqrt(number); i++)
			if (number % i == 0) {
				return "Not a prime number";

			}

		return "prime number";
	}
}

// int hcf = 1;
// 36, 24
// 6
// for -- 6 -- if( i=2 36%i == 0 && 24%==0 ) //
// hcf *= i;