package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

// i/p
//2
//15 105 
//24 36

//o/p
//15
//12

public class HCF {
	public static void main(String a[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Total Lines");
		int totalLines = sc.nextInt();
		while (totalLines > 0) {
			System.out.print("Enter 1st No:");
			int n1 = sc.nextInt();
			System.out.print("Enter 2nd No:");
			int n2 = sc.nextInt();
			System.out.println("HCF " + findHcf(n1, n2));
			totalLines--;
		}

		sc.close();

	}

	public static int findHCF(int n1, int n2) {
		int finalHcf = 1;
		for (int i = 2; i < Math.max(n1, n2); i++) {
			if (n1 % i == 0 && n2 % i == 0) {
				finalHcf *= i;
				n1 /= i;
				n2 /= i;
			}
		}
		return finalHcf;
	}

	public static int findHcf(int n1, int n2) {
		int temp;
		while (n1 > 0) { // 36 >0
			temp = n1; // temp = 36
			n1 = n2 % n1; // 24%36 n2 = 24
			n2 = temp; // n1 = 36
		}
		return n2;
	}

	public static int findGCD(int n1, int n2) {
		return 0;
	}
}
