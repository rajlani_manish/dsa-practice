package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class MaxNumber {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Numbers");
		int number1 = sc.nextInt();
		int number2 = sc.nextInt();
		int number3 = sc.nextInt();
		System.out.println("Numbers " + number1 + " " + number2 + " " + number3);
		System.out.println("Largest " + findLargest(number1, number2, number3));
		System.out.println("Minimum " + findMinimum(number1, number2, number3));
		sc.close();
		// findMax(number1, number2, number3);
	}
	
	public static int findLargest(int n1, int n2, int n3) {

		return n1 > (n2 > n3 ? n2 : n3) ? n1 : (n3 > n2 ? n3 : n2);

	}

	public static int findMinimum(int n1, int n2, int n3) {

		return n1 < (n2 < n3 ? n2 : n3) ? n1 : (n3 < n2 ? n3 : n2);

	}

	private static void findMax(int number1, int number2, int number3) {
		if (number1 >= 1 && number1 <= 1000000 && number2 >= 1 && number2 <= 1000000 && number3 >= 1
				&& number3 <= 1000000) {
			if (number1 > number2 && number1 > number3) {
				System.out.println("Max Num - " + number1);
			} else if (number2 > number1 && number2 > number3) {
				System.out.println("Max Num - " + number2);
			} else {
				System.out.println("Max Num - " + number3);
			}
			
		}
	}
	
	
}
