package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

public class EvenNumbers {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int tillNumber = sc.nextInt();

		for (int i = 1; i <= tillNumber; i++) {
			if (i % 2 == 0) {
				System.out.println("Even numbers: " + i);
			}
		}

		sc.close();
	}

}
