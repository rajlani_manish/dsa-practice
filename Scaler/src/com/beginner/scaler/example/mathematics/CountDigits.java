package com.beginner.scaler.example.mathematics;

import java.util.Scanner;

/* Name of the class has to be "Main" only if the class is public. */
class CountDigits
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner sc = new Scanner(System.in);
		//System.out.println("Enter iterations ::"); 
		int iterations = sc.nextInt();
		
		while(iterations > 0) {
			int count = 0;
		      //System.out.println("Enter a number ::"); //123
		      int num = sc.nextInt();
		      do {
		    	  num = num/10; // 123/10
			         //System.out.println(num);
			         count++;
		      } while(num != 0);
		      
		      System.out.println("Number of digits in the entered integer are :: "+count);
		      iterations --;
		}
		sc.close();
	   /*Scanner sc = new Scanner(System.in);
	   System.out.println("Enter Inputs: ");
        String input = sc.nextLine();
        //System.out.println(input);
        
        //for(int i = 0; i < input ; i++ ) {
        	 // String inp = sc.next();        	
        	 // System.out.println(inp);
        	// String s = Integer.toString(inp);
             System.out.println("O/p :" +  input.length());

       // }
        sc.close();*/
        
	}
	
	public void anotherMethod()
	{
		Scanner sc = new Scanner(System.in);
	      int count = 0;
	      System.out.println("Enter a number ::"); //123
	      int num = sc.nextInt();
	      while(num!=0){ // 123!=0
	         num = num/10; // 123/10
	         System.out.println(num);
	         count++;
	      }
	      System.out.println("Number of digits in the entered integer are :: "+count);
	      sc.close();
	}
}

