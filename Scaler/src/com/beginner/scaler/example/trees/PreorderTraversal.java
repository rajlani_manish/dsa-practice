package com.beginner.scaler.example.trees;

import java.util.ArrayList;
import java.util.List;

public class PreorderTraversal {

	public static void main(String[] args) {
		TreeNode tree = new TreeNode(1);
		tree.left = new TreeNode(6);
		tree.right = new TreeNode(2);
		tree.right.left = new TreeNode(3);
	    System.out.println(preorderTraversal(tree));
		/*
		 * Input 1:

			   1
			    \
			     2
			    /
			   3
			   
			Input 2:
			
			   1
			  / \
			 6   2
			    /
			   3
					 * 
					 * Explanation 1:

 The Preoder Traversal of the given tree is [1, 2, 3].
Explanation 2:

 The Preoder Traversal of the given tree is [1, 6, 2, 3].
					 */  

	}
	
	public static ArrayList<Integer> preorderTraversal(TreeNode A) {
		ArrayList<Integer> list = new ArrayList<>();
		preOrder(A, list);
		return list;
    }
	
	private static void preOrder(TreeNode root, List<Integer> list)
	{
		if(root == null)
			return;
		list.add(root.val);
		preOrder(root.left, list);
		preOrder(root.right, list);		
		
	}

}

/*
 * Q2. Preorder Traversal
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a binary tree, return the preorder traversal of its nodes values.

NOTE: Using recursion is not allowed.



Problem Constraints

1 <= number of nodes <= 105



Input Format

First and only argument is root node of the binary tree, A.



Output Format

Return an integer array denoting the preorder traversal of the given binary tree.



Example Input

Input 1:

   1
    \
     2
    /
   3
Input 2:

   1
  / \
 6   2
    /
   3


Example Output

Output 1:

 [1, 2, 3]
Output 2:

 [1, 6, 2, 3]


Example Explanation

Explanation 1:

 The Preoder Traversal of the given tree is [1, 2, 3].
Explanation 2:

 The Preoder Traversal of the given tree is [1, 6, 2, 3].*/
