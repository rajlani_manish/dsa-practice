package com.beginner.scaler.example.trees;

public class CountNodesHasValueGtAncestor {

	public static void main(String[] args) {
		TreeNode tree = new TreeNode(4);
		tree.left = new TreeNode(5);
		tree.right = new TreeNode(2);
		tree.right.left = new TreeNode(3);
		tree.right.right = new TreeNode(6);
		System.out.println(solve(tree));
	}
	
	public static int solve(TreeNode A) {		
		int maxSoFar = A.val;
		return countNodesGreaterThanAncestor(A, maxSoFar) + 1;
	}
	
	private static int countNodesGreaterThanAncestor(TreeNode A, int maxSoFar) {
		int noOfNodesGreaterThanAncestor = 0;
		if(A == null)
			return 0;
		if(A.val > maxSoFar)
		{
			maxSoFar = A.val;
			System.out.println("Visited..");
			noOfNodesGreaterThanAncestor++;
		}
		noOfNodesGreaterThanAncestor += countNodesGreaterThanAncestor(A.left, maxSoFar);
		noOfNodesGreaterThanAncestor += countNodesGreaterThanAncestor(A.right, maxSoFar);
		return noOfNodesGreaterThanAncestor;	
	}
	
	

}

/*
 * Q2. Counting the Nodes*/

/*Problem Description

Given the root of a tree A with each node having a certain value, find the count of nodes which have more value than all its ancestor



Problem Constraints

1 <= Number of Nodes <= 200000

1 <= Value of Nodes <= 2000000000



Input Format

First and only argument of input is a tree node.



Output Format

Return a single integer denoting count of nodes which have more value than all of it's ancestor.



Example Input

Input 1:

 
     3
Input 2:

 
    4
   / \
  5   2
     / \
    3   6


Example Output

Output 1:

 1
Output 2:

 3


Example Explanation

Explanation 1:

 One node is valid
Explanation 2:

 Three nodes are 4, 5 and 6.
 */
