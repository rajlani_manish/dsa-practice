package com.beginner.scaler.example.trees;

import java.time.LocalDate;
import java.time.temporal.IsoFields;

public class BSTNodesRange {
	
	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		System.out.println(date);
		System.out.println(date.get(IsoFields.QUARTER_OF_YEAR));
		//System.out.println(bst.solve(null, 0, 0));
		TreeNode B = new TreeNode(8);
		B.left = new TreeNode(6);
		B.left.left = new TreeNode(1);
		B.left.right = new TreeNode(4);
		B.right = new TreeNode(21);
		
		System.out.println(solve(B, 2, 20));
		System.out.println(solve2(B, 2, 20));
		
		
		/*
		 *      8
	           / \
	          6  21
	         / \
	        1   4	

     B = 2
     C = 20
		 * 
		 */
	}
	
	public static int solve(TreeNode A, int start, int end) {
		if (A == null)
			return 0;
		if (A.val < start)
			return solve(A.right, start, end);
		if (A.val > end)
			return solve(A.left, start, end);
		return 1  + solve(A.left, start, end) + solve(A.right, start, end);
	}
	
	public static int solve2(TreeNode A, int start, int end) { // not working
		int count = 0;
		if (A == null)
			return 0;
		if (A.val >= start && A.val <= end)
			count++;
		solve(A.left, start, end);
		solve(A.right, start, end);
		return count;
	}

}

/*
Q3. BST nodes in a range
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a binary search tree of integers. You are given a range B and C.

Return the count of the number of nodes that lies in the given range.



Problem Constraints

1 <= Number of nodes in binary tree <= 100000

0 <= B < = C <= 109



Input Format

First argument is a root node of the binary tree, A.

Second argument is an integer B.

Third argument is an integer C.



Output Format

Return the count of the number of nodes that lies in the given range.



Example Input

Input 1:

            15
          /    \
        12      20
        / \    /  \
       10  14  16  27
      /
     8

     B = 12
     C = 20
Input 2:

            8
           / \
          6  21
         / \
        1   4

     B = 2
     C = 20


Example Output

Output 1:

 5
Output 2:

 3


Example Explanation

Explanation 1:

 Nodes which are in range [12, 20] are : [12, 14, 15, 20, 16]
Explanation 2:

 Nodes which are in range [2, 20] are : [8, 6, 4]*/