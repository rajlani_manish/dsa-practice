package com.beginner.scaler.example.trees;

import java.util.ArrayList;

public class PostorderTraversal {
	public static void main(String[] args) {
		TreeNode tree = new TreeNode(1);
		tree.left = new TreeNode(6);
		tree.right = new TreeNode(2);
		tree.right.left = new TreeNode(3);
		System.out.println(postorderTraversal(tree));
	}

	private static ArrayList<Integer> postorderTraversal(TreeNode A) {
		ArrayList<Integer> list = new ArrayList<>();
		postOrder(A, list);
		return list;
	}

	private static void postOrder(TreeNode root, ArrayList<Integer> list) {
		if (root == null)
			return;
		postOrder(root.left, list);
		postOrder(root.right, list);
		list.add(root.val);
	}
}

/*
Q3. Postorder Traversal
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a binary tree, return the Postorder traversal of its nodes values.

NOTE: Using recursion is not allowed.



Problem Constraints

1 <= number of nodes <= 105



Input Format

First and only argument is root node of the binary tree, A.



Output Format

Return an integer array denoting the Postorder traversal of the given binary tree.



Example Input

Input 1:

   1
    \
     2
    /
   3
Input 2:

   1
  / \
 6   2
    /
   3


Example Output

Output 1:

 [3, 2, 1]
Output 2:

 [6, 3, 2, 1]


Example Explanation

Explanation 1:

 The Preoder Traversal of the given tree is [3, 2, 1].
Explanation 2:

 The Preoder Traversal of the given tree is [6, 3, 2, 1].*/