package com.beginner.scaler.example.trees;

public class SymmetricBinaryTree {

	public static void main(String[] args) {
		TreeNode A = new TreeNode(1);
		A.left = new TreeNode(2);
		A.right = new TreeNode(2);
		A.left.left = new TreeNode(3);
		A.left.right = new TreeNode(4);
		A.right.left = new TreeNode(4);
		A.right.right = new TreeNode(3);
			/*
			 *  1
			   / \
			  2   2
			 / \ / \
			3  4 4  3
			 */
		System.out.println(isSymmetric(A));

	}

	 public static int isSymmetric(TreeNode A) {
		 if(null == A)
			 return 1;
		 return isSym(A.left, A.right) ? 1 : 0;
	    }
	 
	 private static boolean isSym(TreeNode A, TreeNode B)
	 {		
		 if(null == A && null == B)
			 return true;
		 if(null == A || null == B)
			 return false;
		 if(A.val != B.val)
			 return false;
		 
		 return isSym(A.left, B.right) && isSym(A.right, B.left);
	 }
	
}

/*
 * Q2. Symmetric Binary Tree
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).



Problem Constraints

1 <= number of nodes <= 105



Input Format

First and only argument is the root node of the binary tree.



Output Format

Return 0 / 1 ( 0 for false, 1 for true ).



Example Input

Input 1:

    1
   / \
  2   2
 / \ / \
3  4 4  3
Input 2:

    1
   / \
  2   2
   \   \
   3    3


Example Output

Output 1:

 1
Output 2:

 0


Example Explanation

Explanation 1:

 The above binary tree is symmetric. 
Explanation 2:

The above binary tree is not symmetric.
 * */
