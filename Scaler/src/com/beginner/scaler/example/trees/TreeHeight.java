package com.beginner.scaler.example.trees;

public class TreeHeight {

	public static void main(String[] args) {
		TreeNode tree = new TreeNode(1);
		tree.left = new TreeNode(2);
		tree.right = new TreeNode(3);
		tree.right.left = new TreeNode(4);
		/* following is the tree after above statement
		 
        1
      /   \
      2   3   
		 /
		4 */
		System.out.println(height(tree));

	}

	public static int height(TreeNode A) {
		// post order traversal applied here
		if (A == null)
			return 0;
		int leftH = height(A.left); // get left Height
		int rightH = height(A.right); // get right Height
		return 1 + Math.max(leftH, rightH); // get max of left and right + root node(1)

	}

}

/*
 * Q4. Tree Height
Solved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

You are given the root node of a binary tree A, You have to find the height of the given tree.

A binary tree's height is the number of nodes along the longest path from the root node down to the farthest leaf node.



Problem Constraints

1 <= Number of nodes in the tree <= 105

0 <= Value of each node <= 109



Input Format

First and only argument is a tree node A.



Output Format

Return an integer denoting the height of the tree.



Example Input

Input 1:

 Values =  1 
          / \     
         4   3                        
Input 2:

 
 Values =  1      
          / \     
         4   3                       
        /         
       2                                     


Example Output

Output 1:

 2 
Output 2:

 3 


Example Explanation

Explanation 1:

 Distance of node having value 1 from root node = 1
 Distance of node having value 4 from root node = 2 (max)
 Distance of node having value 3 from root node = 2 (max)
Explanation 2:

 Distance of node having value 1 from root node = 1
 Distance of node having value 4 from root node = 2
 Distance of node having value 3 from root node = 2
 Distance of node having value 2 from root node = 3 (max)*/
