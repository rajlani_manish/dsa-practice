package com.beginner.scaler.example.trees;

import java.util.ArrayList;
import java.util.List;

public class InorderTraversal {	
	public static void main(String[] args) {
		TreeNode tree = new TreeNode(1);
		tree.left = new TreeNode(6);
		tree.right = new TreeNode(2);
		tree.right.left = new TreeNode(3);
	    System.out.println(inorderTraversal(tree));
		/*
		 * Input 1:

			   1
			    \
			     2
			    /
			   3
			   
			Input 2:
			
			   1
			  / \
			 6   2
			    /
			   3
					 
					 * 
					 * Explanation 1:

 The Inorder Traversal of the given tree is [1, 3, 2].
Explanation 2:

 The Inorder Traversal of the given tree is [6, 1, 3, 2].
					 * 
					 */    
	}

	/**
	 * Definition for binary tree
	 * class TreeNode {
	 *     int val;
	 *     TreeNode left;
	 *     TreeNode right;
	 *     TreeNode(int x) {
	 *      val = x;
	 *      left=null;
	 *      right=null;
	 *     }
	 * }
	 */
	public static ArrayList<Integer> inorderTraversal(TreeNode root) {
		ArrayList<Integer> list = new ArrayList<Integer>();
        if(root == null)
            return list;
        
        inOrder(root, list);
        return list;
    }
    
    public static void inOrder(TreeNode root, List<Integer> list){
        if(root == null)
            return;
        
        inOrder(root.left,list);
        list.add(root.val);
        inOrder(root.right,list);
    }
	
	/*
	 * public int[] inorderTraversal1(TreeNode A) { int[] output = new int[7]; if(A
	 * == null) return; inorderTraversal(A.left); output[]; (A.val)
	 * inorderTraversal(A.right); }
	 */
}

/*
 * Q1. Inorder Traversal
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a binary tree, return the inorder traversal of its nodes values.

NOTE: Using recursion is not allowed.



Problem Constraints

1 <= number of nodes <= 105



Input Format

First and only argument is root node of the binary tree, A.



Output Format

Return an integer array denoting the inorder traversal of the given binary tree.



Example Input

Input 1:

   1
    \
     2
    /
   3
Input 2:

   1
  / \
 6   2
    /
   3


Example Output

Output 1:

 [1, 3, 2]
Output 2:

 [6, 1, 3, 2]


Example Explanation

Explanation 1:

 The Inorder Traversal of the given tree is [1, 3, 2].
Explanation 2:

 The Inorder Traversal of the given tree is [6, 1, 3, 2].*/
