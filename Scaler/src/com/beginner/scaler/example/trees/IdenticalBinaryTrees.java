package com.beginner.scaler.example.trees;

public class IdenticalBinaryTrees {

	public static void main(String[] args) {
		TreeNode A = new TreeNode(1);
		A.left = new TreeNode(2);
		A.right = new TreeNode(3);

		TreeNode B = new TreeNode(1);
		B.left = new TreeNode(2);
		B.right = new TreeNode(3);
		
		System.out.println(isSameTree(A, B));
		System.out.println(isSameTreeforBoolean(A, B));
	}

	public static int isSameTree(TreeNode A, TreeNode B) {
		if (null == A && null == B)
			return 1;
		if (null == A || null == B)
			return 0;
		if(A.val != B.val)
			return 0;
		return (isSameTree(A.left, B.left) == 1 && isSameTree(A.right, B.right) == 1) ? 1 : 0; 
	}
	
	private static boolean isSameTreeforBoolean(TreeNode A, TreeNode B)
	{
		if (null == A && null == B)
			return true;
		if (null == A || null == B)
			return false;
		if(A.val != B.val)
			return false;
		return  isSameTreeforBoolean(A.left, B.left) && isSameTreeforBoolean(A.right, B.right); 
	}
}

/*
 * Q1. Identical Binary Trees
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given two binary trees, check if they are equal or not.

Two binary trees are considered equal if they are structurally identical and the nodes have the same value.



Problem Constraints

1 <= number of nodes <= 105



Input Format

First argument is a root node of first tree, A.

Second argument is a root node of second tree, B.



Output Format

Return 0 / 1 ( 0 for false, 1 for true ) for this problem.



Example Input

Input 1:

   1       1
  / \     / \
 2   3   2   3
Input 2:

   1       1
  / \     / \
 2   3   3   3


Example Output

Output 1:

 1
Output 2:

 0


Example Explanation

Explanation 1:

 Both trees are structurally identical and the nodes have the same value.
Explanation 2:

 Value of left child of the tree is different.*/
