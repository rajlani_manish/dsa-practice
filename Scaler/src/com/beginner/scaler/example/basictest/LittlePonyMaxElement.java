package com.beginner.scaler.example.basictest;

public class LittlePonyMaxElement {

	public static void main(String[] args) {

		int[] A = { 2, 4, 3, 1, 5 };
		int B = 5;

		boolean elementPresent = false;
		for (int i = 0; i < A.length; i++) {
			if (B == A[i]) {
				elementPresent = true;
				break;
			}
		}

		int count = 0;
		for (int i = 0; i < A.length; i++) {
			if (!elementPresent)
				count = -1;

			else if (elementPresent && B < A[i])
				count++;

		}
		System.out.println(count);

	}

}
