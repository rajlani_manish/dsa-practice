package com.beginner.scaler.example.basictest;

import java.util.Scanner;

public class BankAccount {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int totalAmount = sc.nextInt();
		int numOfOperations = sc.nextInt();

		for (int i = 1; i <= numOfOperations; i++) {
			int operation = sc.nextInt();
			int amount = sc.nextInt();
			if (operation == 1) {
				totalAmount = totalAmount + amount; // 1000
				System.out.print(totalAmount);
			} else if (operation == 2) {
				if (totalAmount >= amount) {
					totalAmount = totalAmount - amount;
					System.out.print(totalAmount);
				} else {
					System.out.print("Insufficient Funds");
				}
			}
		}
		sc.close();
	}

}
