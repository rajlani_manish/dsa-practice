package com.beginner.scaler.example.patterns;

import java.util.Scanner;

public class InvertedHalfPyramid {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number");
		int rows = sc.nextInt();

		if (rows >= 1 && rows <= 100) {
			for (int i = 0; i < rows; i++) {
				for (int k = 0; k < rows - i; k++) {
					System.out.print("*");
				}

				System.out.println("");
			}
		}
		sc.close();
	}
}

/*Enter Number5
*****
****
***
**
*
*/