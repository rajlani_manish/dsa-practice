package com.beginner.scaler.example.patterns;

import java.util.Scanner;

public class PrintTraingle {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int input = sc.nextInt();
		printTrangle(input);
		sc.close();
	}

	private static void printTrangle(int n) {
		for (int i = 1; i <= n; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(i);
			}
			System.out.println();
		}
	}

}

/*
 * Print Triangle Given an integer 'n' (0 < n < 10), print the following
 * triangle.
 * 
 * Input : 4
 * 
 * Output:
 * 
 * 1
 * 
 * 22
 * 
 * 333
 * 
 * 4444
 */
