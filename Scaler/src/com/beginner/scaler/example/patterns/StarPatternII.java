package com.beginner.scaler.example.patterns;

import java.util.Scanner;

public class StarPatternII {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter noOfTimes:");
		int noOfTimes = sc.nextInt();
		for (int i = noOfTimes; i > 0; i--) {
			if (i == 1 || i == noOfTimes)
				for (int j = 1; j <= i; j++) {
					System.out.print("*");
				}
			else {
				for (int j = 1; j <= i; j++) {
					if (j == 1 || j == i)
						System.out.print("*");
					else
						System.out.print(" ");
				}
			}
			System.out.println();
		}
		sc.close();
	}

}



/*
 * Star Pattern II Problem Description
 * 
 * Write a program to input an integer N from user and print hollow inverted
 * right triangle star pattern of N lines using '*'.
 * 
 * See example for clarifications.
 * 
 * Problem Constraints 1 <= N <= 1000
 * 
 * Input Format First line is an integer N
 * 
 * Output Format N lines conatining only char '*' as per the question.
 * 
 * Example Input Input 1:
 * 
 * 4 Input 2:
 * 
 * 7
 * 
 * Example Output Output 1:
 ****
 *
		****
		* *
		**
		*
 * Output 2:
		*******
		*    *
		*   *
		*  *
		* *
		**
		*
 **/