package com.beginner.scaler.example.patterns;

import java.util.Scanner;

public class FullPyramid {

	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number");
		int number = sc.nextInt();
		
		if( number >=2 && number <= 100) {
			for(int i= 0 ; i < number; i++)
			{
				for(int j = number- i; j > 1; j--)
				{
					System.out.print(" ");
				}
				for(int k= 0; k<=i ; k++) {
					System.out.print("* ");
				}
				System.out.println("");
			}
		}
		sc.close();
	}
}

//Enter Number6
//	   * 
//    * * 
//   * * * 
//  * * * * 
// * * * * * 
//* * * * * * 





