package com.beginner.scaler.example.patterns;

import java.util.Scanner;

public class StairPattern {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number");
		int N = sc.nextInt();

		if (N >= 1 && N <= 100) {
			for (int i = 0; i < N; i++) {
				for (int j = 0; j <= i; j++) {
					System.out.print("*");
				}
				System.out.println("");
			}
			
		}
		sc.close();
	}
}

//Enter Number5
//*
//**
//***
//****
//*****
