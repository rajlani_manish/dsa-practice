package com.beginner.scaler.example.patterns;

import java.util.Scanner;

public class InvertedFullPyramid {

	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Number");
		int number = sc.nextInt();

		if (number >= 2 && number <= 100) {
			for (int i = 0; i < number; i++) {
				for (int k = 0; k < i; k++) {
					System.out.print(" ");
				}
				for (int j = number - i; j > 0; j--) {
					System.out.print("^ ");
				}

				System.out.println("");
			}
		}
		sc.close();
	}
}

// Enter Number5
// * * * * *
// * * * *
// * * *
// * *
// *
