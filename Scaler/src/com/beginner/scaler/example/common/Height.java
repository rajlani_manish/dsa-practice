//Write a program to input from user a float value(A) representing the height of persons.
//
//You have to print the category of that person.
//If the height is greater than or equal to 195 then that person is "abnormal".
//If the height is in range of [165 -195) then that person is "taller".
//If the height is in range of [150 -165) then that person is "average".
//If the height is smaller than 150 then that person is "dwarf".

package com.beginner.scaler.example.common;

import java.util.Scanner;

public class Height {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Height:");
		float height = sc.nextFloat();
		if (height >= 50 && height <= 250) {
			if (height < 150) {
				System.out.print("dwarf");
			} else if (height >= 150 && height < 165) {
				System.out.print("average");
			} else if (height >= 165 && height < 195) {
				System.out.print("taller");
			} else {
				System.out.print("abnormal");
			}
		}
		sc.close();
	}

}
