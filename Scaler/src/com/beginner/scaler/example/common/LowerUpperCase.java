package com.beginner.scaler.example.common;

import java.util.Scanner;

public class LowerUpperCase {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter char ");

		char ch = sc.nextLine().charAt(0);

		System.out.println((int) ch);

		if (ch >= 'A' && ch <= 'Z') {
			System.out.println("Upper Case");
		} else if (ch >= 'a' && ch <= 'z') {
			System.out.println("Lower Case");
		} else {
			System.out.println("Others");
		}

		sc.close();

	}
}
