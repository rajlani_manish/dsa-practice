package com.beginner.scaler.example.sorting;

import java.util.Arrays;

public class SortByColor {

	public static void main(String[] args) {
		int[] A = { 2, 0, 0, 1, 0, 0, 2, 2, 1, 1, 0, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 2, 2, 2, 0, 0, 1, 0, 2, 1, 1, 2, 1, 2, 2, 1, 0, 2, 2, 1, 1, 1, 0, 1, 0, 1, 0, 2, 1, 2, 0, 2, 0, 1, 1, 0, 2, 2, 1, 2, 0, 2, 1, 1, 1, 2, 0, 1, 0, 2, 2, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 2, 1, 1, 0, 2, 1, 2, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 1, 1, 0, 2, 1, 2, 2, 2, 1, 2, 2, 0, 1, 0, 1, 2, 1, 1, 0, 1, 2, 0, 1, 0, 2, 2, 1, 2, 1, 0, 2, 2, 1, 1, 0, 2, 1, 2 };
		System.out.println(Arrays.toString(sortColors4(A)));

	}
	
	public static int[] sortColors3(int[] A) {
		 int left = 0, right = A.length-1, curr =0;
	        while(curr <= right){
	            if(A[curr] == 0){
	                A[curr] = A[left];
	                A[left] = 0;
	                left++;
	                curr++;
	            } else if(A[curr] == 2){
	                A[curr] = A[right];
	                A[right] = 2;
	                right--;
	            } else {
	                curr++;
	            }
	        }
		return A;
	}
	public static int[] sortColors4(int[] A) {
		int low = 0;
		int high = A.length - 1;
		int mid = 0;
		while(mid < high)
		{
			if(A[mid] == 0)
				{
				swap(A, low, mid);
				low++; mid++;
				}
			
			else if(A[mid] == 2)
			{
				swap(A, mid, high);
				high--;
			}			
			
			else
				mid++;
		}
		return A;
	}
	
	private static void swap(int[] A, int a, int b) {
		int temp = A[a];
		A[a] = A[b];
		A[b] = temp;
	}
	
	public static int[] sortColors2(int[] A) {
		int low = 0;
		int high = A.length - 1;
		int mid = 0;
		while(mid <= high)
		{
			switch (A[mid]) {
			case 0: {
				A[low] = A[low] ^ A[mid]; // temp = a[i]
				A[mid] = A[low] ^ A[mid]; // a[i] = a[j]
				A[low] = A[low] ^ A[mid]; // a[j] = temp
				low++;
				mid++;
				break;
			}
			case 1: {
				mid++;
				break;
			}
			case 2: {
				A[mid] = A[mid] ^ A[high]; // temp = a[i]
				A[high] = A[mid] ^ A[high]; // a[i] = a[j]
				A[mid] = A[mid] ^ A[high]; // a[j] = temp
				high--;
				break;
			}
			}
		}
		return A;
	}

	public static int[] sortColors1(int[] A) {
		// int temp = 0; 
		for (int i = 0; i < A.length; i++) {     
            for (int j = i+1; j < A.length; j++) {     
               if(A[i] > A[j]) {    
                   A[i] = A[i]^A[j];    // temp = a[i] 
                   A[j] = A[i]^A[j];    // a[i] = a[j]
                   A[i] = A[i]^A[j];    // a[j] = temp
               }     
            }     
        }    
		return A;
	}

}

/*
 * Q1. Sort by Color
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.
Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

Note: Using library sort function is not allowed.



Problem Constraints

1 <= N <= 1000000
0 <= A[i] <= 2


Input Format

First and only argument of input contains an integer array A.


Output Format

Return an integer array in asked order


Example Input

Input 1 :
    A = [0 1 2 0 1 2]
Input 2:

    A = [0]


Example Output

Output 1:
    [0 0 1 1 2 2]
Output 2:

    [0]


Example Explanation

Explanation 1:
    [0 0 1 1 2 2] is the required order.
 * */
