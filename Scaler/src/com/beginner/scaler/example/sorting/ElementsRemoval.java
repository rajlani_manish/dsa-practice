package com.beginner.scaler.example.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ElementsRemoval {

	public static void main(String[] args) {
		int A[] = {1,2};
		System.out.println(solve(A));
		
		Integer B[] = {1,2};
		ArrayList<Integer> al = new ArrayList<Integer>(Arrays.asList((B)));
		System.out.println(solve1(al));
	}
	
	/*
	 * public static int solveBF(int[] A) { Integer[] wrapperArray = new
	 * Integer[A.length];
	 * 
	 * for (int i = 0; i < A.length; i++) { wrapperArray[i] = Integer.valueOf(A[i]);
	 * } Arrays.sort(wrapperArray, Collections.reverseOrder()); int costOfRemoval =
	 * 0; for(int i = 0 ; i < wrapperArray.length; i++) {
	 * 
	 * } }
	 */

	public static int solve(int[] A) {
		Integer[] wrapperArray = new Integer[A.length];
		 
        for (int i = 0; i < A.length; i++) {
        	wrapperArray[i] = Integer.valueOf(A[i]);
        }
		Arrays.sort(wrapperArray, Collections.reverseOrder());
		int costOfRemoval = 0;
		// A = 4,2,1
		// sort - 4, 2, 1
		// 4*1 + 2*2 + 1*3 
		for(int i =0; i< wrapperArray.length; i++)
		{
			costOfRemoval+= wrapperArray[i]*(i+1);
		}
		return costOfRemoval;
    }
	
	public static int solve1(ArrayList<Integer> A) {
		// A = 4,2,1
		// sort - 1, 2, 4
		// 1*3 + 2*2 + 4*1
		Collections.sort(A);
		int sum = 0;

		for (int i = 0; i < A.size(); i++) {
			sum += (A.size() - i) * A.get(i);
		}

		return sum;
	}
}

/*Q2. Elements Removal
 * 
 
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given an integer array A of size N. In one operation, you can remove any element from the array, and the cost of this operation is the sum of all elements in the array present before this operation.

Find the minimum cost to remove all elements from the array.



Problem Constraints

0 <= N <= 1000
1 <= A[i] <= 103



Input Format

First and only argument is an integer array A.



Output Format

Return an integer denoting the total cost of removing all elements from the array.



Example Input

Input 1:

 A = [2, 1]
Input 2:

 A = [5]


Example Output

Output 1:

 4
Output 2:

 5


Example Explanation

Explanation 1:

 Given array A = [2, 1]
 Remove 2 from the array => [1]. Cost of this operation is (2 + 1) = 3.
 Remove 1 from the array => []. Cost of this operation is (1) = 1.
 So, total cost is = 3 + 1 = 4.
 
Explanation 2:

 There is only one element in the array. So, cost of removing is 5.

 */