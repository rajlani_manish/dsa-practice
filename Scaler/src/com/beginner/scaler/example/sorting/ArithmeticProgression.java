package com.beginner.scaler.example.sorting;

import java.util.ArrayList;
import java.util.Arrays;

public class ArithmeticProgression {

	public static void main(String[] args) {
		int B[] = {1, 2};
		int[] A = {3, 5, 2};
		System.out.println(solve(A));
		System.out.println(solve(B));

	}
	
	public static int solve(int[] A) {
		if (A.length == 2)
			return 1;
		Arrays.sort(A);
		int diff = A[1] - A[0];
		for (int i = 2; i < A.length ; i++) {
			int currDiff = A[i] - A[i-1];
			if(currDiff != diff)
				return 0;
		}
		return 1;
    }
	
	public static int solve(ArrayList<Integer> A) {
		return 1;
    }

}

/*
Q3. Arithmetic Progression?
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given an integer array A of size N. Return 1 if the array can be rearranged to form an arithmetic progression, otherwise, return 0.

A sequence of numbers is called an arithmetic progression if the difference between any two consecutive elements is the same.



Problem Constraints

2 <= N <= 105

-109 <= A[i] <= 109



Input Format

First and only argument is an integer array A of size N.



Output Format

Return 1 if the array can be rearranged to form an arithmetic progression, otherwise, return 0



Example Input

Input 1:

 A = [3, 5, 1]
Input 2:

 A = [2, 4, 1]


Example Output

Output 1:

 1
Output 2:

 0


Example Explanation

Explanation 1:

 We can reorder the elements as [1,3,5] or [5,3,1] with differences 2 and -2 respectively, between each consecutive elements.
Explanation 2:

 There is no way to reorder the elements to obtain an arithmetic progression.
*/