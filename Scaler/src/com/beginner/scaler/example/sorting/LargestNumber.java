package com.beginner.scaler.example.sorting;

import java.util.Arrays;
import java.util.Comparator;

public class LargestNumber {

	public static void main(String[] args) {
		int A[] = { 2, 3, 9, 0 };
		int C[] = {3,30,34,5,9};
		int B[] = {0, 0};
		System.out.println(largestNumber(C));
	}

	public static String largestNumber(final int[] A) {		
		LargestNumberSolution s = new LargestNumberSolution();
		System.out.println(s.largestNumber(A));
		String[] arr = new String[A.length];
		for (int i = 0; i < A.length; i++) {
			arr[i] = A[i] + "";
		}
		// Arrays.sort(arr); // [3, 30, 34, 5, 9] // 9 5 34 30 3 but 9534330 is bigger
		// so need to write own logic for sorting
		// System.out.println(Arrays.toString(arr));
		Arrays.sort(arr, (a, b) -> {
			long n1 = Long.parseLong(a + b); // need long to compare n1, n2 which one is big
			long n2 = Long.parseLong(b + a);
			if (n1 > n2)
				return 1;
			else if (n2 > n1)
				return -1;
			else
				return 0;

		});
		//without lambda approach
		Arrays.sort(arr, new Comparator<String>() {
	        @Override
	        public int compare(String a, String b) {
	        	long n1 = Long.parseLong(a + b); // need long to compare n1, n2 which one is big
	        	long n2 = Long.parseLong(b + a);
				if (n1 > n2)
					return 1;
				else if (n2 > n1)
					return -1;
				else
					return 0;
	        }
	    });
		if (arr[0].equals("0")) // if [0, 0] then o/p wud be only 0
			return "0";
		StringBuilder sb = new StringBuilder("");
		for (int i = arr.length - 1; i >= 0; i--) {
			sb.append(arr[i]);
		}

		return sb.toString();
	}
	//Time complexity :O(nlgn) - sort
	// Space complexity :(n)O(n) - for string array

}

/*
 * Q1. Largest Number Unsolved character backgroundcharacter Stuck somewhere?
 * Ask for help from a TA & get it resolved Get help from TA Problem Description
 * 
 * Given a array A of non negative integers, arrange them such that they form
 * the largest number.
 * 
 * Note: The result may be very large, so you need to return a string instead of
 * an integer.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= len(A) <= 100000 0 <= A[i] <= 2*109
 * 
 * 
 * 
 * Input Format
 * 
 * First argument is an array of integers.
 * 
 * 
 * 
 * Output Format
 * 
 * Return a string representing the largest number.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [3, 30, 34, 5, 9] Input 2:
 * 
 * A = [2, 3, 9, 0]
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * "9534330" Output 2:
 * 
 * "9320"
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * A = [3, 30, 34, 5, 9] Reorder the numbers to [9, 5, 34, 3, 30] to form the
 * largest number. Explanation 2:
 * 
 * Reorder the numbers to [9, 3, 2, 0] to form the largest number 9320.
 */
