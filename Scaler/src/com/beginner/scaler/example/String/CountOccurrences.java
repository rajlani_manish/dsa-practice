package com.beginner.scaler.example.String;

public class CountOccurrences {

	public static void main(String[] args) {
		System.out.println(solveBf("abobc"));
		System.out.println(solveBf("bobob"));
	}
	
	public static int solveBf(String A) {
		int subStringCount = 0;
		for(int i = 0 ; i<= A.length(); i++)
		{
			for(int j = i+1; j <= A.length(); j++)
			{
				String substring = A.substring(i, j);
				if(substring.equals("bob"))
					subStringCount++;
			}
		}
		return subStringCount;
    }
	
	public static int solveOptimized(String A) {
		int subStringCount = 0;
		for(int i = 0 ; i<= A.length(); i++)
		{
			for(int j = i+1; j <= A.length(); j++)
			{
				String substring = A.substring(i, j);
				if(substring.equals("bob"))
					subStringCount++;
			}
		}
		return subStringCount;
    }

}

/*

Q2. Count Occurrences
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Find number of occurrences of bob in the string A consisting of lowercase english alphabets.



Problem Constraints

1 <= |A| <= 1000


Input Format

The first and only argument contains the string A consisting of lowercase english alphabets.


Output Format

Return an integer containing the answer.


Example Input

Input 1:

  "abobc"
Input 2:

  "bobob"


Example Output

Output 1:

  1
Output 2:

  2


Example Explanation

Explanation 1:

  The only occurrence is at second position.
Explanation 2:

  Bob occures at first and third position.
*/