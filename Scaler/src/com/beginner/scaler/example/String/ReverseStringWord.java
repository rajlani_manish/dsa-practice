package com.beginner.scaler.example.String;

public class ReverseStringWord {

	public static void main(String[] args) {
		System.out.println(solve1("I am Manish"));

	}
	
	 public static String solve1(String A) {
		 String a[] = A.split(" ");
		 String str = "";
		 for(int i = a.length - 1; i >=0; i--)
		 {
			 str= str + a[i] + " ";
		 }
		 return str;
	    }

}

/*
 * Q1. Reverse the String
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a string A of size N.

Return the string A after reversing the string word by word.

NOTE:

A sequence of non-space characters constitutes a word.
Your reversed string should not contain leading or trailing spaces, even if it is present in the input string.
If there are multiple spaces between words, reduce them to a single space in the reversed string.


Problem Constraints

1 <= N <= 3 * 105



Input Format

The only argument given is string A.



Output Format

Return the string A after reversing the string word by word.



Example Input

Input 1:
    A = "the sky is blue"
Input 2:
    A = "this is ib"  


Example Output

Output 1:
    "blue is sky the"
Output 2:
    "ib is this"    


Example Explanation

Explanation 1:
    We reverse the string word by word so the string becomes "the sky is blue".
Explanation 2:
    We reverse the string word by word so the string becomes "this is ib".
 */