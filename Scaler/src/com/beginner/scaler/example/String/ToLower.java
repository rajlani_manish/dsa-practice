package com.beginner.scaler.example.String;

public class ToLower {

	public static void main(String[] args) {
		char[] A= {'S', 'c', 'A', 'l', 'e', 'r', 'A', 'c', 'a', 'D', 'e', 'm', 'y'};
		System.out.println(to_lower(A));

	}
	
	public static char[] to_lower(char[] A) {
		for(int i = 0; i<A.length; i++)
		{
			if(A[i] >= 'A' && A[i] <= 'Z' )
			{
				A[i]^=32;
			}
		}
		return A;
    }

}
