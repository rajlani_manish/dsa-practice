package com.beginner.scaler.example.String;

public class SimpleReverse {

	public static void main(String[] args) {
		System.out.println(solve1("Geeks"));
		System.out.println(reverse("Manish"));
	}

	public static String solve(String A) {
		char ch ;
		String reverse = "" ;
		for (int i=0; i<A.length(); i++)
	      {
	        ch= A.charAt(i); //extracts each character
	        reverse= ch + reverse; //adds each character in front of the existing string
	      }
		return reverse;
    }
	
	public static String solve1(String A) {
		StringBuilder output = new StringBuilder();
		char[] input = A.toCharArray();
		int l = 0, r = A.length() - 1;
		while (l < r) {
			input[l] = (char) (input[l] ^ input[r]);
			input[r] = (char) (input[l] ^ input[r]);
			input[l] = (char) (input[l] ^ input[r]);			
			l++;
			r--;
		}
		output.append(input);
		return output.toString() ;
    }
	
	private static String reverse(String A)
	{
		StringBuilder str = new StringBuilder(A);
		for(int i = 0; i < str.length()/2; i++)
		{
			int front = i;
			int back = str.length() - 1 - i;
			
			char frontChar = str.charAt(front);
			char backChar = str.charAt(back);
			
			str.setCharAt(front, backChar);
			str.setCharAt(back, frontChar);
		}
		
		return str.toString();
	}
	
	
	
}

/*
 * Q2. Simple Reverse
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a string A, you are asked to reverse the string and return the reversed string.



Problem Constraints

1 <= |A| <= 105

String A consist only of lowercase characters.



Input Format

First and only argument is a string A.



Output Format

Return a string denoting the reversed string.



Example Input

Input 1:

 A = "scaler"
Input 2:

 A = "academy"


Example Output

Output 1:

 "relacs"
Output 2:

 "ymedaca"


Example Explanation

Explanation 1:

 Reverse the given string.
 * */
