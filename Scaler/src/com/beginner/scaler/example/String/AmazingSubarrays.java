package com.beginner.scaler.example.String;

public class AmazingSubarrays {

	public static void main(String[] args) {
		System.out.println(solve("ABEC"));

	}

	public static int solve(String A) {
		int noOfAmazingSubArrays = 0;
		char[] input = A.toCharArray();
		for (int i = 0; i < A.length(); i++) {
			if (input[i] == 'a' || input[i] == 'e' || input[i] == 'i' || input[i] == 'o' || input[i] == 'u'
					|| input[i] == 'A' || input[i] == 'E' || input[i] == 'I' || input[i] == 'O' || input[i] == 'U')
				noOfAmazingSubArrays += A.length() - i;
			//If there is vowel at ith index there will be n-i subarrays starting with vowel from i
		}
		return noOfAmazingSubArrays % 10003;
	}
}

/*
 * Q1. Amazing Subarrays
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
You are given a string S, and you have to find all the amazing substrings of S.

Amazing Substring is one that starts with a vowel (a, e, i, o, u, A, E, I, O, U).

Input

Only argument given is string S.
Output

Return a single integer X mod 10003, here X is number of Amazing Substrings in given string.
Constraints

1 <= length(S) <= 1e6
S can have special characters
Example

Input
    ABEC

Output
    6

Explanation
    Amazing substrings of given string are :
    1. A
    2. AB
    3. ABE
    4. ABEC
    5. E
    6. EC
    here number of substrings are 6 and 6 % 10003 = 6.
    */
