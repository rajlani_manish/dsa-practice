package com.beginner.scaler.example.String;

public class IsAlnum {

	public static void main(String[] args) {
		char[] A = { 'S', 'c', 'a', 'l', 'e', 'r', 'A', 'c', 'a', 'd', 'e', 'm', 'y', '2',
				'0', '2', '0' };
		char[] B = {'S', 'c', 'a', 'l', 'e', 'r', '#', '2', '0', '2', '0'};
		System.out.println(solve(B));

	}
	
	public static int solve(char[] A) {
		int i = 0;
		while (i < A.length) {
			if (!(A[i] >= '0' && A[i] <= '9') && !(A[i] >= 'a' && A[i] <= 'z') && !(A[i] >= 'A' && A[i] <= 'Z')) {
				return 0;
			}
			i++;
		}
		return 1;
    }

}
