package com.beginner.scaler.example.stackqueues;

import java.util.Stack;

public class BalancedParantheses2 {

	public static void main(String[] args) {
		System.out.println(solve("(()())"));

	}
	
	public static int solve(String A) {
		Stack<Character> st = new Stack<Character>();
		for (int i = 0; i < A.length(); i++) {
			if (A.charAt(i) == '(') {
				st.push(A.charAt(i));
			} else if (st.size() > 0 && st.peek() == '(') {
				st.pop();
			} else
				return 0;

		}

		if (st.size() == 0)
			return 1;
		return 0;
	}

}

/*
 * Q2. Balanced Parantheses! Unsolved character backgroundcharacter Stuck
 * somewhere? Ask for help from a TA & get it resolved Get help from TA Problem
 * Description
 * 
 * Given a string A consisting only of '(' and ')'.
 * 
 * You need to find whether parantheses in A is balanced or not ,if it is
 * balanced then return 1 else return 0.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= |A| <= 105
 * 
 * 
 * 
 * Input Format
 * 
 * First argument is an string A.
 * 
 * 
 * 
 * Output Format
 * 
 * Return 1 if parantheses in string are balanced else return 0.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = "(()())" Input 2:
 * 
 * A = "(()"
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * 1 Output 2:
 * 
 * 0
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * Given string is balanced so we return 1 Explanation 2:
 * 
 * Given string is not balanced so we return 0
 */