package com.beginner.scaler.example.Recursion;

public class Output7Recursion {

	public static void main(String[] args) {
		System.out.println(output(2, 10));

	}

	private static int output(int x, int n) {
		if (n == 0)
			return 1;
		else if (n % 2 == 0)
			return output(x * x, n / 2);
		else
			return x * output(x * x, (n - 1) / 2);
	}

}
