package com.beginner.scaler.example.Recursion;

import java.util.Scanner;

public class ReverseString {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		//String rev=reverseString(str);
		ReverseString rev = new ReverseString();
		
		// 1st approach 
		char[] input = str.toCharArray();
      //  rev.reverseString111(input, input.length - 1);
        
        //2nd approach
       // reverseString222(str);
        
        
        // 3rd approach
		/*
		 * StringBuilder sb = new StringBuilder(str);
		 * 
		 * int end = sb.length()-1;
		 * 
		 * helper(sb, end);
		 */
        StringBuilder sb = new StringBuilder(str);
        //reverseString444(sb, 0, sb.length()-1);
        
        // method 5
        

        reverseString555(str, 0);
		sc.close();
	}
	
	public void reverseString111(char[] str, int end) {	// working	
		if (end == 0) {
			System.out.print(str[end]);
			return;
		}
		System.out.print(str[end]);
		reverseString111(str, end-1);
	}
	
	public static void reverseString222(String a){
        if(a.length() == 1)
        {
            System.out.print(a);
            return;
        }

        System.out.print(a.charAt(a.length()-1));
        reverseString222(a.substring(0, a.length()-1));
    }  
	
	private static String reverseString(String str)
	{
		char[] input = str.toCharArray();
		//reverseString444(input, 0, input.length-1);
		//StringBuilder output = new StringBuilder();
		//output.append(input);
		//System.out.println(output);
		return "";
	}
	
	public static void reverseString333(StringBuilder sb, int end){ // working
        if(end == 0)
        {
            System.out.println(sb.charAt(end));
            return;
        }
        System.out.print(sb.charAt(end));
        reverseString333(sb, end-1);
    }
	
	private static void reverseString444(StringBuilder sb, int start, int end) { //notworking
		if (start >= end) {
			System.out.print(sb.charAt(start));
			return;
		}

		char c = sb.charAt(start); //temp = a;
		sb.setCharAt(start, sb.charAt(end)); //a = b;
		sb.setCharAt(end, c); // b = temp;

		
		reverseString444(sb, start + 1, end - 1);
		System.out.print(sb.charAt(start));
	}
	
	 public static void reverseString555(String a, int i){
	        if(i == a.length()-1)
	        {
	            System.out.print(a.charAt(i));
	            return;
	        }

	        reverseString555(a, i+1);
	        System.out.print(a.charAt(i));
	    }
	

}
