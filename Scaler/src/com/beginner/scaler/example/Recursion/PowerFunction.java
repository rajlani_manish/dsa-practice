package com.beginner.scaler.example.Recursion;

public class PowerFunction {

	public static void main(String[] args) {
		System.out.println(pow(2, 3, 3));
		System.out.println(pow(71045970, 41535484, 64735492));
	}
	
	public static int pow(int A, int B, int C) {
		if(A==0)
			return 0;
        if(B==0)
            return 1;
        if(A < 0 && B == 1)
            return ((A + C) % C); 

        int hp = pow(A, B/2, C);
        int ha = (int)((((long)(hp%C)) * ((long)(hp%C)))%C);
        if(B%2==0)
        	return ha;
        else return (int)((((long)(ha%C) * ((long)(A%C)))% C));
    }

}

/*
 * TestCase - Trivial Case Correctness Failed
Wrong Answer
Your program's output doesn't match the expected output. You can try testing your code with custom input and try putting debug statements in your code.
Your submission failed for the following input
A : 0
B : 0
C : 1

Test As Custom Input
The expected return value:
0
Your function returned the following:
1
*/

/*

Q1. Implement Power Function
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Implement pow(x, n) % d.
In other words, given x, n and d,

find (xn % d)

Note that remainders on division cannot be negative. In other words, make sure the answer you return is non negative.

Input : x = 2, n = 3, d = 3
Output : 2

2^3 % 3 = 8 % 3 = 2.

*/