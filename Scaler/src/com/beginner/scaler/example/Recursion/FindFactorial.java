package com.beginner.scaler.example.Recursion;

public class FindFactorial {

	public static void main(String[] args) {
		System.out.println(solve(5));

	}
	
	 public static int solve(int A) {
		 if(A==0) 
			 return 1; // 0! = 1
		 return A * solve (A-1);
	    }

}
