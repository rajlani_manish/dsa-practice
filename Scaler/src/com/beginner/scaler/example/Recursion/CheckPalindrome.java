package com.beginner.scaler.example.Recursion;

public class CheckPalindrome {

	public static void main(String[] args) {
		System.out.println(solve("naman"));
		System.out.println(solve("aa"));
		//System.out.println(isPalindrome("naman", 0, 4));
	}
	
	public static int solve(String A) {
		char[] input = A.toCharArray();
		int start = 0;
		int end = A.length() - 1;
		//return isPalindrome(A) ? 1 : 0;
		return palindrome_check(input, start, end);
	}
	
	private static boolean isPalindrome(String str) 
    { 
        //If string has 0 or 1 character
        if(str.length() == 0 || str.length() == 1)
            return true; 
        //If string has multiple characters
        //Check whether first and last characters are same or not
        if(str.charAt(0) == str.charAt(str.length()-1))
            return isPalindrome(str.substring(1, str.length()-1));
        return false;
    } 
	
	private static int palindrome_check(char [] s, int start, int end)
    {
        if(start > end)        
            return 1;       
        else
        {
            if(s[start]==s[end])            
                return palindrome_check(s,start+1,end-1);            
        }   
        return 0;        
    }
}

/*
Q1. Check Palindrome
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Write a recursive function that checks whether a string A is a palindrome or Not.
Return 1 if the string A is palindrome, else return 0.

Note: A palindrome is a string that's the same when reads forwards and backwards.



Problem Constraints

1 <= A <= 50000

String A consist only of lowercase letters.



Input Format

First and only argument is a string A.



Output Format

Return 1 if the string A is palindrome, else return 0.



Example Input

Input 1:

 A = "naman"
Input 2:

 A = "strings"


Example Output

Output 1:

 1
Output 2:

 0


Example Explanation

Explanation 1:

 "naman" is a palindomic string, so return 1.
Explanation 2:

 "strings" is not a palindrome, so return 0.
*/