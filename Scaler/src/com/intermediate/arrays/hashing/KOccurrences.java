package com.intermediate.arrays.hashing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class KOccurrences {

	public static void main(String[] args) {
		ArrayList<Integer> A = new ArrayList<Integer>();
		Collections.addAll(A, new Integer[] { 1, 2, 2, 3, 3 });
		System.out.println(getSum(5, 2, A));

		ArrayList<Integer> B = new ArrayList<Integer>();
		Collections.addAll(B, new Integer[] { 2, 2, 2, 2 });
		System.out.println(getSum(4, 4, B));

		ArrayList<Integer> C = new ArrayList<Integer>();
		Collections.addAll(C, new Integer[] { 0, 0, 2 });
		System.out.println(getSum(3, 2, C));

		ArrayList<Integer> D = new ArrayList<Integer>();
		Collections.addAll(D, new Integer[] { 1, 2, 3, 4, 5 });
		System.out.println(getSum(5, 5, D));
	}

	public static int getSum(int A, int B, ArrayList<Integer> C) {
		int sum = 0;
		HashMap<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < C.size(); i++) {
			if (map.containsKey(C.get(i)))
				map.put(C.get(i), map.get(C.get(i)) + 1);
			else
				map.put(C.get(i), 1);
		}
		System.out.println(map);

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() == B) {
				sum += entry.getKey();
			}
		}

		if (sum == 0 && map.containsKey(0) && map.get(0) == B)
			return 0;

		return (sum > 0) ? (sum % 1000000007)  : -1;

	}

}
