package com.intermediate.arrays.hashing;

import java.util.HashMap;

public class FirstRepeatingElement {
	// First Repeating Element | Java Solution | TC - O(N) | SC: O(N)
	public static void main(String[] args) {
		int A[] = {10, 5, 3, 4, 3, 5, 6};
		int B[] = {6, 10, 5, 4, 9, 120};
		//System.out.println(solve(A));
		//System.out.println(solve(B));
		
		int C[] = { 1, 3, 6, 10,  15,  10,   20,  10,   21,   10 } ;
		System.out.println(solve(C));
	}
	
	public static int solve(int[] A) {
		 int firstRepeatingElement = -1;
			HashMap<Integer, Integer> map = new HashMap<>();
			
	        // add all array elements into hahmap
			for (int i = 0; i < A.length; i++) { // O(N)
				if (map.containsKey(A[i]))
					map.put(A[i], map.get(A[i]) + 1);
				else
					map.put(A[i], 1);
			}
			//System.out.println(map);
			
	        // Iterate over the array in order and check if that element has freq > 1 (repeating element)
			for(int i= 0 ; i < A.length; i++)
			{ // O(N)
				if (map.containsKey(A[i]) && map.get(A[i]) > 1) {
					firstRepeatingElement = A[i];
					break;
				}
			}
			
			return firstRepeatingElement;
			
			// O(N)
    }

}


