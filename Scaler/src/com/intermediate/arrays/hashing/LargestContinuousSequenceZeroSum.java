package com.intermediate.arrays.hashing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

public class LargestContinuousSequenceZeroSum {

	public static void main(String[] args) {
		ArrayList<Integer> A = new ArrayList<>();
		Collections.addAll(A, new Integer[] {1,2,-2,4,-4});
		//System.out.println(lszero(A));		
		
		ArrayList<Integer> B = new ArrayList<>();
		Collections.addAll(B, new Integer[] {1,2,-3, 3});
		System.out.println(lszero(B));
		
		ArrayList<Integer> C = new ArrayList<>();
		Collections.addAll(C, new Integer[] {-19, 8, 2, -8, 19, 5, -2, -23});
		//System.out.println(lszero(C));
		
		ArrayList<Integer> D = new ArrayList<>();
		Collections.addAll(D, new Integer[] {22, -7, 15, -21, -20, -8, 16, -20, 5, 2, -15, -24, 19, 25, 3, 23, 18, 0, 16, 26, 13, 4, -20, -13, -25, -2});
		//System.out.println(lszero(D));
		 
		
	}
	
	public static ArrayList<Integer> lszero(ArrayList<Integer> A) {
		System.out.println(A);
		ArrayList<Integer> output = new ArrayList<Integer>();
		int sum = 0;
		//ArrayList<Integer> ps = new ArrayList<Integer>();
		int start = -1, end = -1;
		int maxDistance = -1;
		
		HashMap<Integer, Integer> map = new HashMap<>();
		map.put(0, -1);
		for (int i = 0; i < A.size(); i++) {
			sum += A.get(i).intValue();
			// ps.add(sum);
			if (map.containsKey(sum)) {
				int index = map.get(sum); // 1,2,-2,4,-4
				//                           1 3 1 5 1
				// 0, -1
				//1, 0
				//3, 1
				// 0, 3
				// 1,2,-3, 3
				// 1,3,0, 3
				int currDistance = i - (index + 1) + 1; // 3
				if (currDistance > maxDistance) { // 3> -1
					maxDistance = currDistance; // 3
					start = index + 1; // 0
					end = i;
				}

			} else
				map.put(sum, i);
		}		
		
		System.out.println("start " + start);
		System.out.println("end " + end);

		if (start > -1 && end > -1) {
			for (int i = start; i <= end; i++) {
				output.add(A.get(i));
			}
		}

		return output;
    }

}

/*
 * Q3. Largest Continuous Sequence Zero Sum
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given an array A of N integers.

Find the largest continuous sequence in a array which sums to zero.



Problem Constraints

1 <= N <= 106

-107 <= A[i] <= 107



Input Format

Single argument which is an integer array A.



Output Format

Return an array denoting the longest continuous sequence with total sum of zero.

NOTE : If there are multiple correct answers, return the sequence which occurs first in the array.



Example Input

A = [1,2,-2,4,-4]


Example Output

[2,-2,4,-4]


Example Explanation

[2,-2,4,-4] is the longest sequence with total sum of zero.
*/
