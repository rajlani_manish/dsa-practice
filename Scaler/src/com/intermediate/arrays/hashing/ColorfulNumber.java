package com.intermediate.arrays.hashing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class ColorfulNumber {

	public static void main(String[] args) {
		System.out.println(colorful1(23));
		System.out.println(colorful1(236));

	}

	public static int colorful(int A) {
        HashSet < Integer > hashSet = new HashSet < > ();
        ArrayList < Integer > numbers = new ArrayList < > ();
        while (A != 0) {
            int num = A % 10;
            numbers.add(num);
            A /= 10;
        }
        Collections.reverse(numbers);
        int n = numbers.size();
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                int prod = 1;
                for (int k = i; k <= j; k++) {
                    prod *= numbers.get(k);
                }
                if (hashSet.contains(prod))
                    return 0;
                hashSet.add(prod);
            }
        }
        return 1;
    }
	
	public static int colorful1(int A) {
		int colorful = 1;
		String s = Integer.toString(A);

		// Either prepare the String or get each no from the A and keep adding into
		// arraylist
		// and reverse the same.
		HashSet<Integer> set = new HashSet<>();  // 23
		for (int i = 0; i < s.length(); i++) { // 2 3 23
            for (int j = i; j < s.length(); j++) {
                int prod = 1;
                for (int k = i; k <= j; k++) {
                	// System.out.print(s.charAt(k) - '0');
                    prod *= (s.charAt(k) - '0');
                }
                //System.out.println("prod" + prod);
                // if set already has that product then return false else true
                if (set.contains(prod))
                    return 0;
                set.add(prod);
            }
        }

		return colorful;
	}

}

/*
 * Q3. Colorful Number
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

For Given Number A find if its COLORFUL number or not.

If number A is a COLORFUL number return 1 else return 0.

What is a COLORFUL Number:

A number can be broken into different contiguous sub-subsequence parts. 
Suppose, a number 3245 can be broken into parts like 3 2 4 5 32 24 45 324 245. 
And this number is a COLORFUL number, since product of every digit of a contiguous subsequence is different.


Problem Constraints

1 <= A <= 2 * 109



Input Format

Single Argument which denotes integer A.



Output Format

Return 1 if integer A is COLORFUL else return 0.



Example Input

Input 1:

 A = 23
Input 2:

 A = 236


Example Output

Output 1:

 1
Output 2:

 0


Example Explanation

Explanation 1:

 Possible Sub-sequences: [2, 3, 23] where
 2 -> 2 
 3 -> 3
 23 -> 6  (product of digits)
 This number is a COLORFUL number since product of every digit of a sub-sequence are different. 
Explanation 2:

 Possible Sub-sequences: [2, 3, 6, 23, 36, 236] where
 2 -> 2 
 3 -> 3
 6 -> 6
 23 -> 6  (product of digits)
 36 -> 18  (product of digits)
 236 -> 36  (product of digits)
 This number is not a COLORFUL number since the product sequence 23  and sequence 6 is same. 
 */
