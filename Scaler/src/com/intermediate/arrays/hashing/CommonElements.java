package com.intermediate.arrays.hashing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CommonElements {

	public static void main(String[] args) {
		//int A[] = { 1, 2, 2, 1 };
		//int B[] = { 2, 3, 1, 2 };
		//solve(A, B);
		//System.out.println(solve(A, B));
		ArrayList<Integer> A = new ArrayList<>();		
		Collections.addAll(A, new Integer[] {1, 2, 2, 1});
		ArrayList<Integer> B = new ArrayList<>();
		Collections.addAll(B, new Integer[] {2, 3, 1, 2});
		//System.out.println(solve(A, B));
		
		ArrayList<Integer> C = new ArrayList<>();		
		Collections.addAll(C, new Integer[] {2, 1, 4, 10});
		ArrayList<Integer> D = new ArrayList<>();
		Collections.addAll(D, new Integer[] {3, 6, 2, 10, 10});
		System.out.println(solve(C, D));
		
	}

	/*
	 * public static int[] solve1(int[] A, int[] B) {
	 * 
	 * HashMap<Integer, Integer> mapA = new HashMap<Integer, Integer>();
	 * 
	 * // Iterate 1st array and add into map - // as key - array element and value
	 * as frequency for (int i = 0; i < A.length; i++) { if (mapA.containsKey(A[i]))
	 * mapA.put(A[i], mapA.get(A[i]) + 1); else mapA.put(A[i], 1); }
	 * 
	 * System.out.println(mapA);
	 * 
	 * // Iterate 2nd array and see if its present into the map ArrayList<Integer>
	 * output = new ArrayList<Integer>(); for(int i = 0 ; i < B.length; i++) { if
	 * (mapA.containsKey(B[i])) { mapA.put(A[i], mapA.get(A[i]) - 1);
	 * output.add(B[i]); } } System.out.println(output);
	 * 
	 * // create o/p of same size as updated map //int[] output = new
	 * int[mapA.size()];
	 * 
	 * 
	 * }
	 */
	
	public static ArrayList<Integer> solve(ArrayList<Integer> A, ArrayList<Integer> B) {
		ArrayList<Integer> output = new ArrayList<>();
		HashMap<Integer, Integer> mapA = new HashMap<Integer, Integer>();

		// Iterate 1st array and add into map -
		// as key - array element and value as frequency
		for (int i = 0; i < A.size(); i++) {
			if (mapA.containsKey(A.get(i)))
				mapA.put(A.get(i), mapA.get(A.get(i)) + 1);
			else
				mapA.put(A.get(i), 1);
		}
		//System.out.println(mapA);
		
		//Iterate 2nd array and check if B elements are present into mapA
		// then reduce the frequency of that element into mapA
		// and add same element into output
		for (int i = 0; i < B.size(); i++) {
			// if that element is still present with freq > 0 - mapA.get(B.get(i)) > 0
			if(mapA.containsKey(B.get(i)) && mapA.get(B.get(i)) > 0)
			{
				mapA.put(B.get(i), mapA.get(B.get(i)) - 1);
				output.add(B.get(i));
				System.out.println(output);
			}
		}
		return output;

	}

}
