package com.intermediate.arrays.hashing;

import java.util.HashMap;
import java.util.Map;

public class CheckPalindrome {

	public static void main(String[] args) {
		//System.out.println(solve("abcde"));
		System.out.println(solve("mom"));
		System.out.println(solve("mnxljrajwhxiaquajokwvoqqphy"
				+ "lxpbanmmhfxsmssxzsdnprtgibuhaxnwxzfozexiascybplaaqjcthuy"
				+ "dnoowmkzyamodzknkqmwdglqqnhflfslqyowcangsddhcjjuiyfbdkev"
				+ "lghbictrvnmnathotrekyrggwcmbzorqtyeowksywlbetsyhjvczcnvu"
				+ "sfdrxythrhhoxtuuprqftgwohcgpngktkharijsovuknae"));
	}

	public static int solve(String A) {
		if(A.length() == 1)
			return 1;
		HashMap<Character, Integer> map = new HashMap<>();
		for(int i = 0 ; i < A.length(); i++)
		{
			if(map.containsKey(A.charAt(i)))
			{
				map.put(A.charAt(i), map.get(A.charAt(i)) + 1);
			} else {
				map.put(A.charAt(i), 1);
			}
		}
		System.out.println(map);
		// aba //abaa // abae // abcd // abaee count++;
		//int countOfEvenNoOfChar = 0;
		int countOfOddNoOfChar = 0;
		for (Integer freq : map.values()) {
			/*if (freq % 2 == 0)
				countOfEvenNoOfChar = 1;*/
			if (freq % 2 == 1) {
				countOfOddNoOfChar++;
				if (countOfOddNoOfChar > 1)
					return 0;
			}
		}
		//System.out.println(countOfEvenNoOfChar);
		System.out.println(countOfOddNoOfChar);
		/*
		 * if(countOfOddNoOfChar>= countOfEvenNoOfChar) return 0;
		 */
		
		return 1;
	}

}
