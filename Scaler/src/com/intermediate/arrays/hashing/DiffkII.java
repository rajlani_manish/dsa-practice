package com.intermediate.arrays.hashing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class DiffkII {

	public static void main(String[] args) {
		List<Integer> A = new ArrayList<>();
		Collections.addAll(A, new Integer[] {1, 5, 3});
		System.out.println(diffPossible(A, 2));
		
		List<Integer> B = new ArrayList<>();
		Collections.addAll(B, new Integer[] {11, 85, 100, 44, 3, 32, 96, 72, 93, 76, 67, 93, 63, 5, 10, 45, 99, 35, 13});
		System.out.println(diffPossible(B, 60));

		List<Integer> C = new ArrayList<>();
		Collections.addAll(C, new Integer[] {0});
		System.out.println(diffPossible(C, 0));
		
		// 1 2 1 2 , B = 3 total pairs = 4
		 // A[i] + A[j] = k
	}
	
	public static int diffPossible(final List<Integer> A, int B) {
		
		if(A.size() == 1)
			return 0;
		
		int output = 0;
		
		// A[i] - A[j] = k, i != j
		// A[i] = k + A[j]
		// if for A[j] is present in the set for A[i] means it has got k = A[i] - A[j]
		
		HashSet<Integer> set = new HashSet<>();
        for(int i = 0; i < A.size(); i++)
        {
            if(set.contains(B + A.get(i)) || set.contains(A.get(i) - B))
                return 1;
            else 
                set.add(A.get(i));
        }
        
        return output;
		
    }

}

/*
 * Q2. Diffk II
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Given an array A of integers and another non negative integer k, find if there exists 2 indices i and j such that A[i] - A[j] = k, i != j.

Example :

Input :

A : [1 5 3]
k : 2
Output :

1
as 3 - 1 = 2

Return 0 / 1 for this problem*/
