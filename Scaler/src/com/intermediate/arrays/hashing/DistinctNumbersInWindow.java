package com.intermediate.arrays.hashing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class DistinctNumbersInWindow {

	public static void main(String[] args) {
		ArrayList<Integer> A = new ArrayList<Integer>();
		Collections.addAll(A, new Integer[] { 1, 2, 1, 3, 4, 3 });
		System.out.println(dNums(A, 3));

		ArrayList<Integer> B = new ArrayList<Integer>();
		Collections.addAll(B, new Integer[] { 1, 1, 2, 2 });
		//System.out.println(dNums(A, 1));
	}

	public static ArrayList<Integer> dNums(ArrayList<Integer> A, int B) {
		ArrayList<Integer> output = new ArrayList<Integer>();
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < B; i++) {
			if (map.containsKey(A.get(i)))
				map.put(A.get(i), map.get(A.get(i)) + 1);
			else
				map.put(A.get(i), 1);
		}
		output.add(map.size());
		
		int i = 0, j = B;
		
		while (j < A.size())// 1, 2, 1, 3, 4, 3
		{
			// remove previous element
			if (map.containsKey(A.get(i))) {
				if (map.get(A.get(i)) > 1)
					map.put(A.get(i), map.get(A.get(i)) - 1);
				else
					map.remove(A.get(i));
			}

			// add next element
			if (map.containsKey(A.get(j)))
				map.put(A.get(j), map.get(A.get(j)) + 1);
			else
				map.put(A.get(j), 1);
			
			output.add(map.size());
			i++;
			j++;
		}

		return output;
	}

}

/*
 * Q4. Distinct Numbers in Window
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

You are given an array of N integers, A1, A2 ,�, AN and an integer B. Return the of count of distinct numbers in all windows of size B.

Formally, return an array of size N-B+1 where i'th element in this array contains number of distinct elements in sequence Ai, Ai+1 ,..., Ai+B-1.

NOTE: if B > N, return an empty array.



Input Format

First argument is an integer array A
Second argument is an integer B.



Output Format

Return an integer array.



Example Input

Input 1:

 A = [1, 2, 1, 3, 4, 3]
 B = 3
Input 2:

 A = [1, 1, 2, 2]
 B = 1


Example Output

Output 1:

 [2, 3, 3, 2]
Output 2:

 [1, 1, 1, 1]


Example Explanation

Explanation 1:

 A=[1, 2, 1, 3, 4, 3] and B = 3
 All windows of size B are
 [1, 2, 1]
 [2, 1, 3]
 [1, 3, 4]
 [3, 4, 3]
 So, we return an array [2, 3, 3, 2].
Explanation 2:

 Window size is 1, so the output array is [1, 1, 1, 1].
 */
