package com.intermediate.arrays.subsequencessubsets;

public class SpecialSubsequencesAG {

	public static void main(String[] args) {
		System.out.println(solve("ABCGAG"));
	}

	public static int solve(String A) {
		int agCount = 0;
		int aCount = 0;
		for (int i = 0; i < A.length(); i++) {
			if (A.charAt(i) == 'A')
				aCount++;

			if (A.charAt(i) == 'G')
				agCount += aCount;
		}
		return agCount;
	}

}
