package com.intermediate.arrays.prefixsumcarryforward.assignment;

import java.util.ArrayList;
import java.util.List;

public class SpecialIndexRemovalSumEvenSumOdd {

	public static void main(String[] args) {
		ArrayList<Integer> A = new ArrayList<Integer>();
		// 0  1   2   3   4   5
		// 4  3   2   7   6  -2
		A.add(4);
		A.add(3);
		A.add(2);
		A.add(7);
		A.add(6);
		A.add(-2);		
		solve(A);		
		
		ArrayList<Integer> B = new ArrayList<Integer>();
		// 0 1 2
		// 1 1 1
		B.add(1);
		B.add(1);
		B.add(1);		
		solve(B);
	}
	
	public static int solve(ArrayList<Integer> A) {		
		int n = A.size();
		int countOfSpecialIndex = 0;
		int evenSum = 0, oddSum = 0;		
		
		List<Integer> psEven = prefixSumEven(A);
		List<Integer> psOdd = prefixSumOdd(A);
		
		//			0  1   2   3   4   5
		// 			4  3   2   7   6  -2
		//Even Sum [4, 4, 6, 6, 12, 12]
		//Odd Sum  [0, 3, 3, 10, 10, 8]
	
		// 			 0 1 2
		//			 1 1 1
		// Even Sum [1, 1, 2]
		// Odd Sum [0, 1, 1]
		
		System.out.println("Even Sum " + prefixSumEven(A));
		System.out.println("Odd Sum " + prefixSumOdd(A));
		
		if(psEven.get(A.size()-1) - A.get(0) == psOdd.get(A.size()-1))
		{  // When we remove 0th element then -- > 1 to n-1, 
			countOfSpecialIndex++;
		} if(psEven.get(A.size()-2) == psOdd.get(A.size()-2))
		{
			// When we remove n-1 element then -- > 0 to n-2
			countOfSpecialIndex++;
		}
			
		//System.out.println("countOfSpecialIndex " + countOfSpecialIndex);
		
		for(int i = 1; i < n - 1; i++)
		{
			// range - 0 to i-1           //range- i+1 to n-1
			evenSum =  psEven.get(i-1) + (psOdd.get(n-1) - psOdd.get(i));
			oddSum =   psOdd.get(i-1) + (psEven.get(n-1) - psEven.get(i));
			
			if(evenSum == oddSum)
				countOfSpecialIndex++;
		}
		//System.out.println("countOfSpecialIndex " + countOfSpecialIndex);
		return countOfSpecialIndex;
    }
	
	private static List<Integer> prefixSumEven(ArrayList<Integer> A) {
		List<Integer> psEven = new ArrayList<>();		
		psEven.add(0, A.get(0));
		for (int i = 1; i < A.size(); i++) {
			if (i % 2 == 1)
				psEven.add(i, psEven.get(i - 1));
			else
				psEven.add(i, (psEven.get(i - 1) + A.get(i)));
		}
		return psEven;		
	}
	
	private static List<Integer> prefixSumOdd(ArrayList<Integer> A)
	{
		List<Integer> psOdd = new ArrayList<>();
		psOdd.add(0, 0);
		for (int i = 1; i < A.size(); i++) {
			if (i % 2 == 0)
				psOdd.add(i, psOdd.get(i - 1));
			else
				psOdd.add(i, (psOdd.get(i - 1) + A.get(i)));
		}
		return psOdd;		
	}

}


/*
Q3. Count ways to make sum of odd and even indexed elements equal by removing an array element
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given an array, arr[] of size N, the task is to find the count of array indices such that removing an element from these indices makes the sum of even-indexed and odd-indexed array elements equal.



Problem Constraints

1<=n<=1e5
-1e5<=A[i]<=1e5


Input Format

First argument contains an array A of integers of size N


Output Format

Return the count of array indices such that removing an element from these indices makes the sum of even-indexed and odd-indexed array elements equal.



Example Input

Input 1:
A=[2, 1, 6, 4]
Input 2:

A=[1, 1, 1]


Example Output

Output 1:
1
Output 2:

3


Example Explanation

Explanation 1:
Removing arr[1] from the array modifies arr[] to { 2, 6, 4 } such that, arr[0] + arr[2] = arr[1]. 
Therefore, the required output is 1. 
Explanation 2:

 Removing arr[0] from the given array modifies arr[] to { 1, 1 } such that arr[0] = arr[1] 
Removing arr[1] from the given array modifies arr[] to { 1, 1 } such that arr[0] = arr[1] 
Removing arr[2] from the given array modifies arr[] to { 1, 1 } such that arr[0] = arr[1] 
Therefore, the required output is 3.
*/