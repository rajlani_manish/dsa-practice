package com.intermediate.arrays.prefixsumcarryforward.assignment;

public class ClosestMinMax {

	public static void main(String[] args) {
		// A = 20, 3, 13, 5
		//
		// 20 -
		// 20, 3 + // 2 length
		// 20, 3, 13 + // 3 length
		// 20, 3, 13, 5 + // 4 length
		// 3, -
		// 3, 13 -
		// 3, 13, 5 -
		// 13 -
		// 13 5 -
		// 5 -
		// o/p - 2 smallest length

		int A[] = { 1, 1, 1 };
		int B[] = { 814, 761, 697, 483, 981 };
		int C[] = { 20, 2, 1, 3, 20, 1 };
		 System.out.println(solveBF(A));
		System.out.println(solveBF(B));
		System.out.println(solveBF(C));

	}

	public static int solve(int[] A) { // 
        int minValue = A[0];
        // get the min value of array
		for (int i = 1; i < A.length; i++) {
			if (A[i] < minValue)
				minValue = A[i];
		}
		int maxValue = A[0];
        // get the max value of array
		for (int i = 1; i < A.length; i++) {
			if (A[i] > maxValue)
				maxValue = A[i];
		}
		
		int minLength = Integer.MAX_VALUE;

		int posMin = -1, posMax = -1;
		for (int i = 0; i < A.length; i++) {
			if (A[i] == minValue)
				posMin = i; // if global min found take its position
			if (A[i] == maxValue)
				posMax = i; // if global max found take its position

			if (posMin != -1 && posMax != -1) // if both are found
			{// see min 
				int len = Math.abs(posMin - posMax) + 1;
				minLength = Math.min(minLength, len);
			}

		}
		return minLength;
		// O(N)
    }
	
	public static int solveBF(int[] A) {
		// BF approach
		int min_ele = Integer.MAX_VALUE, max_ele = Integer.MIN_VALUE;
		int ans = Integer.MAX_VALUE;
		for (int x : A) {
			min_ele = Math.min(min_ele, x);
			max_ele = Math.max(max_ele, x);
		}		
		
		for(int i = 0; i < A.length; i++)
		{
			if(A[i] == min_ele) // look for min, if found
			{
				for(int j = i; j < A.length; j++)
				{
					if(A[j]== max_ele) // look for max in rest of the array
						ans = Math.min(ans, j-i+1); // find the length b/w max and min
				}
			} else if(A[i] == max_ele) // look for max, if found
			{
				for(int j = i; j < A.length; j++)
				{
					if(A[j]== min_ele)  // look for min in rest of the array
						ans = Math.min(ans, j-i+1); // find the length b/w max and min
				}
			}
		}
		return ans;
		
	}

	public static int solve1(int[] A) {

		int min_Index = -1, max_Index = -1;
		int min_ele = Integer.MAX_VALUE, max_ele = Integer.MIN_VALUE;

		int ans = Integer.MAX_VALUE;
		for (int x : A) {
			min_ele = Math.min(min_ele, x);
			max_ele = Math.max(max_ele, x);
		}

		for (int i = 0; i < A.length; i++) {
			if (A[i] == min_ele)
				min_Index = Math.max(min_Index, i);
			if (A[i] == max_ele)
				max_Index = Math.max(max_Index, i);

			if (min_Index != -1 && max_Index != -1) {
				int len = Math.abs(max_Index - min_Index) + 1; // +1 for 0 bases index
				ans = Math.min(ans, len);
			}

		}
		return ans;
	}
	
	// 20, 2, 1, 3, 2, 1
	// min = 1 max = 20
	// min len Inte.maxVal
	// loop - max value found - posMax = 0
	// max value found at 0 index
	// min value found posMax = 2
	// both -1 != min and max
	// min leng = min( MAX_VALUE, abs (2-0) +1) == 3
	// minLeng - 3

}

/*
 * Problem Description
 * 
 * Given an array A. Find the size of the smallest subarray such that it
 * contains atleast one occurrence of the maximum value of the array
 * 
 * and atleast one occurrence of the minimum value of the array.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= |A| <= 2000
 * 
 * 
 * 
 * Input Format
 * 
 * First and only argument is vector A
 * 
 * 
 * 
 * Output Format
 * 
 * Return the length of the smallest subarray which has atleast one occurrence
 * of minimum and maximum element of the array
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [1, 3] Input 2:
 * 
 * A = [2]
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * 2 Output 2:
 * 
 * 1
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * Only choice is to take both elements. Explanation 2:
 * 
 * Take the whole array.
 */