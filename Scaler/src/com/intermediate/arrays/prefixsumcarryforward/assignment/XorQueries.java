package com.intermediate.arrays.prefixsumcarryforward.assignment;

import java.util.Arrays;

public class XorQueries {

	public static void main(String[] args) {
		int A[] = { 1, 0, 0, 0, 1 };
		int B[][] = { { 2, 4 }, { 1, 5 }, { 3, 5 } };
		System.out.println(Arrays.deepToString(solve(A, B)));
	}

	public static int[][] solve(int[] A, int[][] B) {
		int[][] output = new int[B.length][B[0].length];
		for (int i = 0; i < B.length; i++) {
			int left = -1, right = -1;
			left = B[i][0];
			right = B[i][1];

			//System.out.println("l " + left + "r " + right);

			int xorFromLtoR = 0;
			int noOfTimes0 = 0;

			for (int j = left - 1; j < right; j++) // -1 coz 1 bases indexing
			{
				xorFromLtoR = xorFromLtoR ^ A[j];

				if (A[j] == 0)
					noOfTimes0++;

			}
			//System.out.println("ansXor " + xorFromLtoR);
			//System.out.println("noOfTimes0 " + noOfTimes0);

			output[i][0] = xorFromLtoR;
			output[i][1] = noOfTimes0;

		}
		return output;
	}
}

/*
 * Q1. Xor queries
Solved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

You are given an array A (containing only 0 and 1) as element of N length.
Given L and R, you need to determine value of XOR of all elements from L to R (both inclusive) and number of unset bits (0's) in the given range of the array.



Problem Constraints

1<=N,Q<=100000
1<=L<=R<=N


Input Format

First argument contains the array of size N containing 0 and 1 only. 
Second argument contains a 2D array with Q rows and 2 columns, each row represent a query with 2 columns representing L and R.


Output Format

Return an 2D array of Q rows and 2 columns i.e the xor value and number of unset bits in that range respectively for each query.


Example Input

A=[1,0,0,0,1]
B=[ [2,4],
    [1,5],
    [3,5] ]


Example Output

[[0 3]
[0 3]
[1 2]]


Example Explanation

In the given case the bit sequence is of length 5 and the sequence is 1 0 0 0 1. 
For query 1 the range is (2,4), and the answer is (array[2] xor array[3] xor array[4]) = 0, and number of zeroes are 3, so output is 0 3. 
Similarly for other queries.
*/
