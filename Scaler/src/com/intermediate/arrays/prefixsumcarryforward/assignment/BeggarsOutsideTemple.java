package com.intermediate.arrays.prefixsumcarryforward.assignment;

import java.util.Arrays;

public class BeggarsOutsideTemple {

	public static void main(String[] args) {
		// Input: N = 5, D = [[1, 2, 10], [2, 3, 20], [2, 5, 25]]
		//
		// 0 0 0 0 0
		// 10 10
		// 30 20
		// 55 45 25 25
		//
		// Return: [10, 55, 45, 25, 25]
		
		int n = 5;
		int[][] input = {
						 {1, 2, 10},
						 {2, 3, 20},
						 {2, 5, 25}
						 };
		System.out.println(Arrays.toString(solve(n, input)));
	}

	public static int[] solve(int A, int[][] B) {
		
		 // Queries - no of rows
		int[] output = new int[A];
		int rows = B.length;
		
		System.out.println("rows : "+ rows);
		// update the queries
		for (int i = 0; i < rows; i++) { // O(N)
			int L = B[i][0] - 1; // -1 coz 1 based indexing
			int R = B[i][1] - 1; // -1 coz 1 based indexing
			int data = B[i][2];
			System.out.println("L : " + L + " R: " + R + " data :" + data);
			
			// keep updating values in the output until N-1
			if (R < A - 1 ) {
				output[L] += data;
				output[R + 1] -= data;
			} else { // for last index A-1 // only update L
				output[L] += data;
			}
			System.out.println("Prefix Sum: " + Arrays.toString(output));
		}
		
		//output
		for (int i = 1; i < A; i++) // will start with 1 coz already covered 0 above
		{ // O(M)
			output[i] = output[i - 1] + output[i];
		}

		return output;
		// TC - O(N+M)
		// SC - O(N) 
	}

}

/*
 * Q1. Beggars Outside Temple Unsolved character backgroundcharacter Stuck
 * somewhere? Ask for help from a TA & get it resolved Get help from TA There
 * are N (N > 0) beggars sitting in a row outside a temple. Each beggar
 * initially has an empty pot. When the devotees come to the temple, they donate
 * some amount of coins to these beggars. Each devotee gives a fixed amount of
 * coin(according to his faith and ability) to some K beggars sitting next to
 * each other.
 * 
 * Given the amount donated by each devotee to the beggars ranging from i to j
 * index, where 1 <= i <= j <= N, find out the final amount of money in each
 * beggar's pot at the end of the day, provided they don't fill their pots by
 * any other means.
 * 
 * Example:
 * 
 * Input: N = 5, D = [[1, 2, 10], [2, 3, 20], [2, 5, 25]]
 * 
 * 0    0   0   0   0
 * 10  10
 *     30   20  
 *     55   45  25  25  
 * 
 * Return: [10, 55, 45, 25, 25]
 * 
 * Explanation: => First devotee donated 10 coins to beggars ranging from 1 to
 * 2. Final amount in each beggars pot after first devotee: [10, 10, 0, 0, 0]
 * 
 * => Second devotee donated 20 coins to beggars ranging from 2 to 3. Final
 * amount in each beggars pot after second devotee: [10, 30, 20, 0, 0]
 * 
 * => Third devotee donated 25 coins to beggars ranging from 2 to 5. Final
 * amount in each beggars pot after third devotee: [10, 55, 45, 25, 25]
 */