package com.intermediate.arrays.prefixsumcarryforward.assignment;

import java.util.Arrays;

public class EquilibriumIndex {

	public static void main(String[] args) {
		int A[] = {-7, 1, 5, 2, -4, 3, 0}; // EquilibriumIndex = 3 element - 2
		int B[] = {0, 0, 0}; // Not having EquilibriumIndex
		int c[] = {1, 2, 3,7, 1, 2, 3};
		//System.out.println(solveWithSpaceO1(c));
		System.out.println(solve(A));
	}

	private static int[] getPrefixSum(int[] A) 
	{		
		int ps[] = new int[A.length];
		ps[0] = A[0];
		for(int i = 1; i< A.length; i++)
		{
			ps[i] =  ps[i-1] + A[i];
		}
		System.out.println("Ps" + Arrays.toString(ps));
		return ps;
		
	}
	
	public static int solveWithSpaceO1(int[] A)
	{
		int equilibriumIndex = -1;
		
		long totalSum = 0;
		for(int i = 0; i < A.length; i++)
		{
			totalSum+= A[i];
		}
		
		//special case when all the elements sum is 0 except the 1st one.
				if(totalSum - A[0] == 0) //  --- {3, 0, 0}
					return 0;
				
		//int ps[] = getPrefixSum(A);
		long sumLeft = A[0], sumRight = 0;
		for (int i = 1; i < A.length; i++) {
			//sumLeft = ps[i - 1]; // 0th element // sumLeft = A[0] + A[i];			
			
			sumRight = totalSum - sumLeft - A[i];
			if (sumLeft == sumRight)
			{
				System.out.println("EquilibriumIndex:" + i);
				return i;
			}
			sumLeft += A[i];
		}
		// this was done in O(N)
		// try to do it in O(N) using maintaining left sum and right sam..
		// see class13 nov class 11:04 PM

		
		// special case - ==> n-1 when suppose we have 10 elements..
		// 1 to 9th sum is 0  and after 10th.. 0.. so 10th is the EI.
		if (equilibriumIndex == -1) {
			if (totalSum - A[A.length - 1] == 0)
				return A.length - 1;
		}
			
		
		return equilibriumIndex;
	}
	
	public static int solve(int[] A) {
		if (A.length == 1) {
			return 0;
		}
		int totalSum = 0;
		for(int i = 0; i < A.length; i++)
		{
			totalSum+= A[i];
		}
		
		//special case when all the elements sum is 0 except the 1st one.
		if(totalSum - A[0] == 0) //  --- {3, 0, 0}
			return 0;
		int equilibriumIndex = -1;
		int ps[] = getPrefixSum(A);
		int sumLeft = 0, sumRight = 0;
		for (int i = 1; i < A.length; i++) {
			sumLeft = ps[i - 1]; // 0th element // sumLeft = A[0] + A[i];
			sumRight = ps[A.length - 1] - ps[i];
			System.out.println("sumLeft: " + sumLeft);
			System.out.println("sumRight: " + sumLeft);
			if (sumLeft == sumRight)
			{
				System.out.println("EquilibriumIndex:" + i);
				return i;
			}
			
		}
		// this was done in O(N)
		// try to do it in O(N) using maintaining left sum and right sam..
		// see class13 nov class 11:04 PM

		
		// special case - ==> n-1 when suppose we have 10 elements..
		// 1 to 9th sum is 0  and after 10th.. 0.. so 10th is the EI.
		if (equilibriumIndex == -1) { // {0, 0, 3}
			if (totalSum - A[A.length - 1] == 0)
				return A.length - 1;
		}
			
		
		return equilibriumIndex;
	}

}

/*
 * Problem Description
 * 
 * You are given an array A of integers of size N.
 * 
 * Your task is to find the equilibrium index of the given array
 * 
 * Equilibrium index of an array is an index such that the sum of elements at
 * lower indexes is equal to the sum of elements at higher indexes.
 * 
 * NOTE:
 * 
 * Array indexing starts from 0. If there is no equilibrium index then return
 * -1. If there are more than one equilibrium indexes then return the minimum
 * index.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1<=N<=1e5 -1e5<=A[i]<=1e5
 * 
 * 
 * Input Format
 * 
 * First arugment contains an array A .
 * 
 * 
 * Output Format
 * 
 * Return the equilibrium index of the given array. If no such index is found
 * then return -1.
 * 
 * 
 * Example Input
 * 
 * Input 1: A=[-7, 1, 5, 2, -4, 3, 0] Input 2:
 * 
 * A=[1,2,3]
 * 
 * 
 * Example Output
 * 
 * Output 1: 3 Output 2:
 * 
 * -1
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1: 3 is an equilibrium index, because: A[0] + A[1] + A[2] = A[4]
 * + A[5] + A[6] Explanation 1:
 * 
 * There is no such index.
 */

//200
