package com.intermediate.arrays.prefixsumcarryforward.assignment;

import java.util.ArrayList;
import java.util.Collections;

public class MinimumAbsoluteDifference {

	public static void main(String[] args) {
		ArrayList<Integer> A = new ArrayList<Integer>();
		// 1, 2, 3, 4, 5
		A.add(1);
		A.add(2);
		A.add(3);
		A.add(4);
		A.add(5);
		System.out.println(solve(A));
		// A= new ArrayList<Integer>();
	}

	public static int solve(ArrayList<Integer> A) {
		Collections.sort(A);
		int minAbsDiff = Integer.MAX_VALUE;
		for (int i = 1; i < A.size(); i++) {
			int ans = Math.abs(A.get(i) - A.get(i-1));
			minAbsDiff = Math.min(minAbsDiff, ans);
		}
		return minAbsDiff;
	}

}

/*
2. Minimum Absolute Difference
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given an array of integers A, find and return the minimum value of | A [ i ] - A [ j ] | where i != j and |x| denotes the absolute value of x.



Problem Constraints

1 <= length of the array <= 100000

-109 <= A[i] <= 109



Input Format

The only argument given is the integer array A.



Output Format

Return the minimum value of | A[i] - A[j] |.



Example Input

Input 1:

 A = [1, 2, 3, 4, 5]
Input 2:

 A = [5, 17, 100, 11]


Example Output

Output 1:

 1
Output 2:

 6


Example Explanation

Explanation 1:

 The absolute difference between any two adjacent elements is 1, which is the minimum value.
Explanation 2:

 The minimum value is 6 (|11 - 5| or |17 - 11|).
*/