package com.intermediate.arrays.matrix;

import java.util.ArrayList;
import java.util.Arrays;

public class MatrixTranspose {

	public static void main(String[] args) {
		// if array is square matrix
		int[][] A = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		System.out.println(Arrays.deepToString(solve(A)));
		// swapping works else not
		//if array is not square matrix
		int[][] B = {{1, 2},{1, 2},{1, 2}};
		System.out.println(Arrays.deepToString(solve(B)));
	}	
	
	public static int[][] solve(int[][] A) {
		int[][] output = new int[A[0].length][A.length]; 
		for(int i = 0; i< A.length; i++)
		{
			for(int j = 0; j< A[i].length; j++)
			{
				output[j][i] = A[i][j];
				//output[j][i] = A[i][j] ^ A[j][i];
				//output[i][j] = A[i][j] ^ A[j][i];
				//swap(A[i][j], A[j][i]);
			}
		}
		return output;
    }
	public ArrayList<ArrayList<Integer>> solve(ArrayList<ArrayList<Integer>> A) {
		ArrayList<ArrayList<Integer>> output = new ArrayList<ArrayList<Integer>>();
		
		return output;
    }

}

/*
 * Q6. Matrix Transpose Unsolved character backgroundcharacter Stuck somewhere?
 * Ask for help from a TA & get it resolved Get help from TA Problem Description
 * 
 * You are given a matrix A, you have to return another matrix which is the
 * transpose of A.
 * 
 * NOTE: Transpose of a matrix A is defined as - AT[i][j] = A[j][i] ; Where 1 ≤
 * i ≤ col and 1 ≤ j ≤ row
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= A.size() <= 1000
 * 
 * 1 <= A[i].size() <= 1000
 * 
 * 1 <= A[i][j] <= 1000
 * 
 * 
 * 
 * Input Format
 * 
 * First argument is vector of vector of integers A representing matrix.
 * 
 * 
 * 
 * Output Format
 * 
 * You have to return a vector of vector of integers after doing required
 * operations.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]] Input 2:
 * 
 * A = [[1, 2],[1, 2],[1, 2]]
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * [[1, 4, 7], [2, 5, 8], [3, 6, 9]] Output 2:
 * 
 * [[1, 1, 1], [2, 2, 2]]
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * Cearly after converting rows to column and columns to rows of [[1, 2, 3],[4,
 * 5, 6],[7, 8, 9]] we will get [[1, 4, 7], [2, 5, 8], [3, 6, 9]].
 */
