package com.intermediate.arrays.matrix;

import java.util.Arrays;

public class AntiDiagonals {

	public static void main(String[] args) {
		int []V = new int[2];
		System.out.println(Arrays.toString(V));
		int A[][] = { 
					  { 1, 2, 3 }, 
					  { 4, 5, 6 } ,
					  { 7, 8, 9 }
					};
		System.out.println(Arrays.deepToString(diagonal(A)));

	}

	public static int[][] diagonal(int[][] A) {
		int n = A.length;
		int m = A[0].length;
		int outputRows = (2 * A.length) - 1;
		int outputColumns = A[0].length;
		int output[][] = new int[outputRows][outputColumns];

		// start from column, keep printing diagonally
		int dRow = 0;
		for (int column = 0; column < m; column++) {
			int row = 0;
			int col = column;
			int dCol = 0;
			while (row < n && col >= 0) {
				output[dRow][dCol] = A[row][col];
				row++;
				dCol++;
				col--;
			}
			dRow++;
		}

		// start from row 1 and last column
		for (int roww = 1; roww < n; roww++) {
			int row = roww;
			int col = m - 1;
			int dCol = 0;
			while (row < n && col >= 0) {
				output[dRow][dCol] = A[row][col];
				row++;
				dCol++;
				col--;
			}
			dRow++;
		}

		return output;
	}
	 

}

/*
 * Q2. Anti Diagonals Unsolved character backgroundcharacter Stuck somewhere?
 * Ask for help from a TA & get it resolved Get help from TA Problem Description
 * 
 * Give a N * N square matrix A, return an array of its anti-diagonals. Look at
 * the example for more details.
 * 
 * 
 * Problem Constraints
 * 
 * 1<= N <= 1000 1<= A[i][j] <= 1e9
 * 
 * 
 * Input Format
 * 
 * First argument is an integer N, denoting the size of square 2D matrix. Second
 * argument is a 2D array A of size N * N.
 * 
 * 
 * Output Format
 * 
 * Return a 2D integer array of size (2 * N-1) * N, representing the
 * anti-diagonals of input array A. The vacant spaces in the grid should be
 * assigned to 0.
 * 
 * 
 * Example Input
 * 
 * Input 1: 3 1 2 3 4 5 6 7 8 9 Input 2:
 * 
 * 1 2 3 4
 * 
 * 
 * Example Output
 * 
 * Output 1: 1 0 0 2 4 0 3 5 7 6 8 0 9 0 0 Output 2:
 * 
 * 1 0 2 3 4 0
 * 
 * 
 * Example Explanation
 * 
 * For input 1: The first anti diagonal of the matrix is [1 ], the rest spaces
 * shoud be filled with 0 making the row as [1, 0, 0]. The second anti diagonal
 * of the matrix is [2, 4 ], the rest spaces shoud be filled with 0 making the
 * row as [2, 4, 0]. The third anti diagonal of the matrix is [3, 5, 7 ], the
 * rest spaces shoud be filled with 0 making the row as [3, 5, 7]. The fourth
 * anti diagonal of the matrix is [6, 8 ], the rest spaces shoud be filled with
 * 0 making the row as [6, 8, 0]. The fifth anti diagonal of the matrix is [9 ],
 * the rest spaces shoud be filled with 0 making the row as [9, 0, 0]. For input
 * 2:
 * 
 * The first anti diagonal of the matrix is [1 ], the rest spaces shoud be
 * filled with 0 making the row as [1, 0, 0]. The second anti diagonal of the
 * matrix is [2, 4 ], the rest spaces shoud be filled with 0 making the row as
 * [2, 4, 0]. The third anti diagonal of the matrix is [3, 0, 0 ], the rest
 * spaces shoud be filled with 0 making the row as [3, 0, 0].
 */
