package com.intermediate.arrays.matrix;

import java.util.ArrayList;
import java.util.Arrays;

public class MatrixMultiplication {

	public static void main(String[] args) {		
		int[][] A = {{1, 2}, {3, 4}}; // 2*2
		int[][] B = {{5, 6}, {7, 8}}; // 2*2
		//System.out.println(Arrays.deepToString(solve(A, B)));
		
		int[][] C = {{1, 1}}; // 1*2
		int D[][] = {{2}, {3}}; // 2*1
		// output - 1*2 ** 2*1 = 1*1
		System.out.println(Arrays.deepToString(solve(C, D)));	
		
	}
	
	public static int[][] solve(int[][] A, int[][] B) {
		int row = A.length;
		int col = B[0].length;
		int row2 = B.length;
		int output[][] = new int[row][col];
		for(int i = 0; i< row; i++)
		{			
			for(int j= 0; j< col; j++)
			{
				output[i][j]= 0;
				for(int k= 0; k< row2; k++)
				{
					output[i][j] += A[i][k] * B[k][j];
				}
			}
		}
		return output;
    }

}

/*
 * Q4. Matrix Multiplication
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

You are given two integer matrices A(having M X N size) and B(having N X P). You have to multiply matrix A with B and return the resultant matrix. (i.e. return the matrix AB).

image



Problem Constraints

1 <= M, N, P <= 100

-100 <= A[i][j], B[i][j] <= 100



Input Format

First argument is a 2D integer matrix A.

Second argument is a 2D integer matrix B.



Output Format

Return a 2D integer matrix denoting AB.



Example Input

Input 1:

 A = [[1, 2],            B = [[5, 6],
      [3, 4]]                 [7, 8]] 
Input 2:

 A = [[1, 1]]            B = [[2],
                              [3]] 


Example Output

Output 1:

 [[19, 22],
  [43, 50]]
Output 2:

 [[5]]


Example Explanation

Explanation 1:

 image
Explanation 2:

 [[1, 1]].[[2, 3]] = [[1 * 2 + 1 * 3]] = [[5]]
 */
