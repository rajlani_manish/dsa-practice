package com.intermediate.arrays.problemsonarrays;

import java.util.Arrays;

public class LengthOfLongestConsecutiveOnes {

	public static void main(String[] args) {
		System.out.println(solve("111011101"));
		System.out.println(solve("010100110101"));
		System.out.println(solve("1101001100101110")); 
		System.out.println(solve("11100"));
		System.out.println(solve("110100"));		
	}

	public static int solve(String A) {
		int ps[] = new int[A.length()];
		int ss[] = new int[A.length()];

		// maintain prefix sum or left sum before the zeros
		ps[0] = A.charAt(0) - '0';
		for (int i = 1; i < A.length(); i++) {
			if (A.charAt(i) - '0' != 0)
				ps[i] = ps[i - 1] + (A.charAt(i) - '0');
		}

		// maintain suffix sum or right sum after the zeros
		ss[A.length() - 1] = A.charAt(A.length() - 1) - '0';
		for (int i = A.length() - 2; i >= 0; i--) {
			if (A.charAt(i) - '0' != 0)
				ss[i] = ss[i + 1] + (A.charAt(i) - '0');
		}

		System.out.println(Arrays.toString(ps));
		System.out.println(Arrays.toString(ss));

		// Get the max length either from the PS or SS coz both will have same no of max
		// 1s
		int maxLength = Integer.MIN_VALUE;
		for (int i = 0; i < ps.length; i++) {
			if (ps[i] > maxLength)
				maxLength = ps[i];
		}

		// get total no of 1s in the given String
		int totalNoOf1s = 0;
		for (int i = 0; i < A.length(); i++) {
			if (A.charAt(i) - '0' == 1)
				totalNoOf1s++;
		}

		int left = -1, right = -1, currentLength = -1;
		// iterate over main string
		for (int i = 0; i < A.length(); i++) {

			// if any 0 found then check the left of it in ps and right of it in ss i.e left
			// and right.
			if (A.charAt(i) - '0' == 0 && i != 0 && i != A.length() - 1) {
				left = ps[i - 1];
				right = ss[i + 1];

				// if total 1s found before 0 and after 0 is less than totalnoOf1s that means it
				// has extra 1 to be replaced
				if (ps[i - 1] + ss[i + 1] < totalNoOf1s)
					currentLength = left + right + 1;

				// if it doesnt have extra 1 to replace then current length will be left + right
				// (either 1s present in left or 1st present in right)
				else
					currentLength = left + right;
				maxLength = Math.max(maxLength, currentLength);
			}
		}

		return maxLength;
	}
	
	public static int solveDintUnderstand(String A) {

		int count = 0, current_count = 0, total_count = 0, zero_count = 0, size = 0;
		int start = 0;
		for (int i = 0; i < A.length(); i++) {
			if (A.charAt(i) == '1')
				count++;
		}
		for (int i = 0; i < A.length(); i++) {
			char ch = A.charAt(i);
			if (ch == '1')
				current_count++;

			if (count > current_count)
				total_count = current_count + zero_count;
			else
				total_count = current_count;
			size = (total_count > size) ? total_count : size;

			if (ch == '0')
				zero_count++;

			while (zero_count == 2) {
				if (A.charAt(start) == '0')
					zero_count--;
				else
					current_count--;
				start++;
			}
		}
		return size;
	}

}

/*
 * Q1. Length of longest consecutive ones Unsolved character backgroundcharacter
 * Stuck somewhere? Ask for help from a TA & get it resolved Get help from TA
 * Given a binary string A. It is allowed to do at most one swap between any 0
 * and 1. Find and return the length of the longest consecutive 1�s that can be
 * achieved.
 * 
 * 
 * Input Format
 * 
 * The only argument given is string A. Output Format
 * 
 * Return the length of the longest consecutive 1�s that can be achieved.
 * Constraints
 * 
 * 1 <= length of string <= 1000000 A contains only characters 0 and 1. For
 * Example
 * 
 * Input 1: A = "111000" Output 1: 3
 * 
 * Input 2: A = "111011101" Output 2: 7
 */