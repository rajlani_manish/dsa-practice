package com.intermediate.arrays.problemsonarrays;

import java.util.ArrayList;

public class RowWithMax1s {

	public static void main(String[] args) {
		
		int A[][] = { 	
						{ 0, 1, 1 }, 
						{ 0, 0, 1 }, 
						{ 0, 1, 1 } 
					};
		int B[][] = { 
						{ 0, 0, 1, 1 }, 
						{ 0, 1, 1, 1 },
						{ 1, 1, 1, 1 } 
					};
		int C[][] = { 
				{ 0, 0, 0}, 
				{ 0, 0, 0 } 
			};
		System.out.println(solve(A));
		System.out.println(solve(B));
		System.out.println(solve(C));

	}
	
	public static int solve(int[][] A) {
		int rowHasMax1s = 0;		
		int row = A.length, col = A[0].length;
		int i = 0, j = col-1; // start from right from 0th row
		while(j>=0 && i<row)
		{
			if(A[i][j]==1)
			{				
				--j;	// go left remove column		
				rowHasMax1s = i;
			}
			
			else if(A[i][j]==0)
				{
				i++;
				//rowHasMax0s = i;
				}
			// go down				
		}
		return rowHasMax1s;
    }
	
	public static int solveForMax0s(int[][] A) {
		int rowHasMax0s = 0;		
		int row = A.length, col = A[0].length;
		int i = 0, j = col-1; // start from right from 0th row
		while(j>=0 && i<row)
		{
			if(A[i][j]==0)
			{				
				// go left 
				j--;
//				{ 0, 0, 1, 1 }, 
//				{ 0, 1, 1, 1 },
//				{ 1, 1, 1, 1 }  
				rowHasMax0s = i;
			}
			
			else if(A[i][j]==1)
				{
				i++;
				}
			// go down				
		}
		return rowHasMax0s;
    }

}

/*
 * Q4. Row with maximum number of ones
Attempted
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

Given a binary sorted matrix A of size N x N. Find the row with the maximum number of 1.

NOTE:

If two rows have the maximum number of 1 then return the row which has a lower index.
Rows are numbered from top to bottom and columns are numbered from left to right.
Assume 0-based indexing.
Assume each row to be sorted by values.
Expected time complexity is O(rows).


Problem Constraints

1 <= N <= 1000

0 <= A[i] <= 1



Input Format

The only argument given is the integer matrix A.



Output Format

Return the row with the maximum number of 1.



Example Input

Input 1:

 A = [   [0, 1, 1]
         [0, 0, 1]
         [0, 1, 1]   ]
Input 2:

 A = [   [0, 0, 0, 0]
         [0, 1, 1, 1]    ]


Example Output

Output 1:

 0
Output 2:

 1


Example Explanation

Explanation 1:

 Row 0 has maximum number of 1s.
Explanation 2:

 Row 1 has maximum number of 1s.*/
