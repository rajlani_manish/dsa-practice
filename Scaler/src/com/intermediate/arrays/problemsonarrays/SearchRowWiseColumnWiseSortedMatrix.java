package com.intermediate.arrays.problemsonarrays;

public class SearchRowWiseColumnWiseSortedMatrix {

	public static void main(String[] args) {
		// ArrayList<ArrayList<Integer>> input = new ArrayList<>();
		// ArrayList<Integer>

		int A[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int B = 2;
		System.out.println(solve(A, B)); // 1011

		int C[][] = { { 2, 8, 8, 8 }, { 2, 8, 8, 8 }, { 2, 8, 8, 8 } };
		int D = 8;
		// System.out.println(solve(C, D));// 1013

		int E[][] = { { 1, 3, 5, 7 }, { 2, 4, 6, 8 } }; // -1 not found
		int F = 10;
		 System.out.println(solve(E, F));

		int G[][] = { { 2, 8, 8, 8 }, 
					  { 2, 8, 8, 8 }, 
					  { 2, 8, 8, 8 } 
					 };
		int H = 8;
		System.out.println(solve(G, H));// 1011 but it returned 1013 // need to return smallest index
	}

	public static int solve(int[][] A, int B) { // O(N + M)
		int i = 0, j = A[0].length - 1;
		int ansFound = -1; int finalAnsFound = Integer.MAX_VALUE;
		while (i < A.length && j >= 0) {
			if (A[i][j] == B) {
				ansFound = ((i + 1) * 1009) + (j + 1); // 1 based indexing
				// if same element found multiple times may be at the lower index
				finalAnsFound = Math.min(finalAnsFound, ansFound);
				j--;
			} else if (A[i][j] > B)
				j--; // remove column
			else if (A[i][j] < B)
				i++; // remove row;
		}
		return finalAnsFound == Integer.MAX_VALUE ? -1 : finalAnsFound;
	}
	// O(N + M)

}
/*
 * Q2. Search in a row wise and column wise sorted matrix Unsolved character
 * backgroundcharacter Stuck somewhere? Ask for help from a TA & get it resolved
 * Get help from TA Problem Description
 * 
 * Given a matrix of integers A of size N x M and an integer B. In the given
 * matrix every row and column is sorted in increasing order. Find and return
 * the position of B in the matrix in the given form: If A[i][j] = B then return
 * (i * 1009 + j) If B is not present return -1.
 * 
 * Note 1: Rows are numbered from top to bottom and columns are numbered from
 * left to right. Note 2: If there are multiple B in A then return the smallest
 * value of i*1009 +j such that A[i][j]=B.
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= N, M <= 1000 -100000 <= A[i] <= 100000 -100000 <= B <= 100000
 * 
 * 
 * Input Format
 * 
 * The first argument given is the integer matrix A. The second argument given
 * is the integer B.
 * 
 * 
 * Output Format
 * 
 * Return the position of B and if it is not present in A return -1 instead.
 * 
 * 
 * Example Input
 * 
 * A = [ [1, 2, 3] [4, 5, 6] [7, 8, 9] ] B = 2
 * 
 * 
 * Example Output
 * 
 * 1011
 * 
 * 
 * Example Explanation
 * 
 * A[1][2]= 2 1*1009 + 2 =1011
 */
