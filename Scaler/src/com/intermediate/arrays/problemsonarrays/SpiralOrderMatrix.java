package com.intermediate.arrays.problemsonarrays;

import java.util.Arrays;

public class SpiralOrderMatrix {

	public static void main(String[] args) {
		//System.out.println(Arrays.deepToString(generateMatrix(3)));
		System.out.println(Arrays.deepToString(generateMatrix(4)));
	}

	public static int[][] generateMatrix(int A) {
		int spiralMatrix[][] = new int[A][A];

		int counter = 1;
		int startRow = 0, startCol = 0;
		int endRow = A - 1 , endCol = A - 1;
		// start inserting 0th row, from column 0 to end
		while (startRow <= endRow && startCol <= endCol) {
			
			for (int i = startCol; i <= endCol; i++) {
				spiralMatrix[startRow][i] = counter++;
			}
			startRow++;
			
			for (int i = startRow ; i <= endRow; i++) {
				spiralMatrix[i][endCol] = counter++;
			}
			endCol--;
			
			 //1 2 3 4
			// 1 2 3 5   
			 //9 8 7 6
			// 9 8 7 6
			     
			for (int i = endCol; i >= startCol; i--) {
				spiralMatrix[endRow][i] = counter++;
			}
			endRow--;
			
			for (int i = endRow; i >= startRow ; i--) {
				spiralMatrix[i][startCol] = counter++;
			}
			startCol++;
			
			System.out.println("startRow " + startRow );
			System.out.println("endRow " + endRow );
			System.out.println("startCol " + startCol );
			System.out.println("endCol " + endCol );
			System.out.println("++++++++++++"); 		 			 			 
			 
		} 		

		return spiralMatrix;
	}
//SR SC   EC
//	 1 2 3
//	 8 9 4
//	 7 6 5
//ER
}

     /*
 * Q3. Spiral Order Matrix II Unsolved character backgroundcharacter Stuck
 * somewhere? Ask for help from a TA & get it resolved Get help from TA Problem
 * Description
 * 
 * Given an integer A, generate a square matrix filled with elements from 1 to
 * A2 in spiral order.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= A <= 1000
 * 
 * 
 * 
 * Input Format
 * 
 * First and only argument is integer A
 * 
 * 
 * Output Format
 * 
 * Return a 2-D matrix which consists of the elements in spiral order.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * 1 Input 2:
 * 
 * 2
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * [ [1] ] Output 2:
 * 
 * [ [1, 2], [4, 3] ]
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * 
 * Only 1 is to be arranged. Explanation 2:
 * 
 * 1 --> 2 | | 4<--- 3
 */