package com.intermediate.example.bitmanipulation;

public class ReverseBits {

	public static void main(String args[]) {        
        System.out.println(reverse(4294967295L));
    }

    public static long reverse(long A) {
        int [] output = store(A);
        for(int i=0,j=31;i<j;i++,j--) {
            output[i]=output[i]^output[j];
            output[j]=output[i]^output[j];
            output[i]=output[i]^output[j];
        }
        return getValue(output);
    }

    public static int[] store(long A) {
        int[] numberBits = new int[32];
        int i=31;
        while(A>0) {
            numberBits[i] = (int)(A%2);
            A=A/2;
            i--;
        }
        return numberBits;
    }

    public static long getValue(int[] A) {
        long ans=0;
        long temp;
        for(int i=31; i>=0;i--) {
        	temp=(long)(A[i] * Math.pow(2,31-i));
            ans+=temp;
        }
        return ans;
    }

}
