package com.intermediate.example.bitmanipulation;

public class SmallestXor {

	public static void main(String args[]) {
	    
        /*
         * Test determineLeftMostBit
         */
//        int leftMostBit;
//        leftMostBit = determineLeftMostBit(0);
//        System.out.println(leftMostBit);
        
        /*
         * Test unsetLeftMostBit
         */
//        int numberAfterUnSet;
//        numberAfterUnSet=unsetLeftMostBit(66,6);
//        System.out.println(numberAfterUnSet);
//        
        
        /*
         * Test setIthBit
         */
//        int numberAftersetIthBit;
//        numberAftersetIthBit=setIthBit(2,6);
//        System.out.println(numberAftersetIthBit);
        
        /*
         * Determine unset bit
         */
        
//        int unSetBit;
//        unSetBit=determineUnsetBit(3);
//        System.out.println(unSetBit);
        
        /*
         * Test final solution
         */
        int X;
        
        X = solve(15,2);
        
        System.out.println("X is : " + X);
        
        
    }
	
	public static int solve(int A, int B) {
        int X=0;
        int tempLeftMostBit;
        int tempUnSetBit;
        while(B>0) {
            tempLeftMostBit = determineLeftMostBit(A);            
            if(tempLeftMostBit!=-1) {
            A=unsetLeftMostBit(A, tempLeftMostBit);
            X=setIthBit(X,tempLeftMostBit);
            }else {
                tempUnSetBit=determineUnsetBit(X);    
                X=setIthBit(X,tempUnSetBit);
            }
            B--;            
        }
        return X;
    }
    
    public static int determineLeftMostBit(int number) {
        int pos=0;
        int leftMostBit=-1;
        while(number>0) {
            if((number&1)==1) {
                leftMostBit=pos;
            }
            pos++;
            number=number>>1;
        }        
        return leftMostBit;
    }
    
    public static int unsetLeftMostBit(int number, int pos) {
        return (1<<pos) ^ number;
    }
    
    public static int setIthBit(int number, int pos) {
        return (1<<pos) | number;
    }
    
    public static int determineUnsetBit(int number) {
        int pos=0;
        while(number>0) {
            if((number&1)==0) {
                break;
            }
            pos++;
            number=(number>>1);
        }
        return pos;
    }

}
