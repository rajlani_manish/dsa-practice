package com.intermediate.example.arrays;

public class SubarrayWithLeastAvg {

	public static void main(String[] args) {
		int A[] = { 3, 7, 5, 20, -10, 0, 12 };
		int B = 2;
		System.out.println(solve(A, B));
		int C[] = {20, 3, 13, 5, 10, 14, 8, 5, 11, 9, 1, 11};
		int D = 9;
		System.out.println(solve(C, D));
	}

	public static int solve(int[] A, int B) { // Used sliding window
		// 
		int currSum = 0;
		int outputIndex = 0;
		for (int i = 0; i < B; i++) { // get sum of 1st k elements
			currSum += A[i]; // O(N) 
		}
		int min = currSum; // store sum of 1st k elements
		for (int i = 1; i <= A.length - B; i++) { // O(N)
			currSum = currSum - A[i - 1] + A[i + B - 1];
			//  A[i - 1] - removing previous element
			//  A[i + B - 1] adding next element
			if (currSum < min) {
				min = currSum;
				outputIndex = i;
			}
		}
		return outputIndex; // O (2N) = O(N)
	}
	//Here we are calculating the sum for each k
	// if sum is less, then automatically the avg will be also less
	//	3, 7, 5, 20, -10, 0, 12*/
	//	/* k= 2 */
	//	3  + 7 = 10
	//	7 + 5 = 12
	//	5 + 20 = 25
	//	20 + -10 = 10
	//	-10 + 0 = -10
	//	0 +  12 = 12
	
}

/*
 * Q10. Subarray with least average
Solved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description
Given an array of size N, Find the subarray with least average of size k.



Problem Constraints
1<=k<=N<=1e5
-1e5<=A[i]<=1e5


Input Format
First argument contains an array A of integers of size N.
Second argument contains integer k.


Output Format
Return the index of the first element of the subarray of size k that has least average.
Array indexing starts from 0.


Example Input
Input 1:
A=[3, 7, 90, 20, 10, 50, 40]
B=3
Input 2:

A=[3, 7, 5, 20, -10, 0, 12]
B=2


Example Output
Output 1:
3
Output 2:

4


Example Explanation
Explanation 1:
Subarray between indexes 3 and 5
The subarray {20, 10, 50} has the least average 
among all subarrays of size 3.
Explanation 2:

 Subarray between [4, 5] has minimum average
 */
