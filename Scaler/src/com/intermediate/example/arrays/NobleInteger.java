package com.intermediate.example.arrays;

import java.util.Arrays;

public class NobleInteger {

	public static void main(String[] args) {
		int A[]= {9,10,10,10,10,10,10,10,10,10};
		int B[] = {3, 2, 1, 3}; // 1, 2, 3, 3
		System.out.println(solve(B));
	}
	
	public static int solve(int[] A) {
        int answer=-1;
        Arrays.sort(A);

        if(A[A.length-1]==0) {
            return 1;
        }

        for(int i=0; i<A.length-1; i++) {
			/*
			 * if(A[i]==A[i+1]) { continue; }
			 */
            if(A[i] == A.length-i-1 && A[i]!=A[i+1]) {
                answer = 1; 
                break;
            }
        }
        return answer;
    }

}
