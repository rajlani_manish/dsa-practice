package com.intermediate.example.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CountDuplicatesRedundant {

	public static void main(String[] args) {
		int[] A = { 1, 10, 20, 1, 25, 1, 10, 30, 25, 1 };
		System.out.println(solve(A));
		System.out.println(solve11(A));
	}

	public static int solve(final int[] A) {
		int redundantCount = 0;
		// auxilary space O(N)
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < A.length; i++) {
			if (map.containsKey(A[i])) {
				map.put(A[i], map.get(A[i]) + 1);
			} else {
				map.put(A[i], 0);
			}
		}

		for (int value : map.values())
			redundantCount += value;

		return redundantCount;
	}
	
	public static int solve11(final int[] A) {
		// O(1) space
        Arrays.sort(A);
        int mainCount = 0;
        System.out.println(Arrays.toString(A));
        for (int i = A.length - 1; i > 0; i--) {
            if (A[i] == A[i - 1]) {
                mainCount += 1;
            }
        }

        return mainCount;
    }

}

/*
 * Q3. Count Duplicates Problem Description
 * 
 * You are given an integer array A having length N. You have to find the number
 * of duplicate(redundant) elements in the array.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= N <= 1000
 * 
 * -109 <= A[i] <= 109
 * 
 * 
 * 
 * Input Format
 * 
 * First and only argument is an integer array A.
 * 
 * 
 * 
 * Output Format
 * 
 * Return a single integer.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [1, 10, 20, 1, 25, 1, 10, 30, 25, 1] Input 2:
 * 
 * A = [1, 2, 3]
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * 5 Output 2:
 * 
 * 0
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * 1 is present 4 times in the array. So, 3 of them are redundant. 10 is present
 * 2 times in the array. So, 1 of them is redundant. 25 is present 2 times in
 * tha array. So, 1 of them is redundant. Total duplicates(redundants) = 3 + 1 +
 * 1 = 5 Explanation 2:
 * 
 * There are no duplicates in the array (Each element has distinct value).
 */