package com.intermediate.example.arrays;

import java.util.Arrays;

public class MultiplicationPreviousNext {

	public static void main(String[] args) {
		int[] A = {1, 2, 3, 4, 5};
		int[] B = {10};
		// Output 1:
	    //[2, 3, 8, 15, 20]
	    if(System.out.printf("Hey") != null)
	    {}
		//System.out.println(Arrays.toString(solve(B)));

	}
	
	public static int[] solve(int[] A) {
		int output[] = new int[A.length];
		if(A.length == 1)
		{
			output = A;
		}
		else
			output[0] = A[0] * A[1];
			output[A.length - 1] = A[A.length - 1] * A[A.length - 2];
			for (int i = 1; i < A.length - 1; i++) {
				output[i] = A[i - 1] * A[i + 1];
				System.out.println(output[i]);
			}
		

		return output;
	}

}

/*
 * Q1. Multiplication of previous and next Unsolved character
 * backgroundcharacter Stuck somewhere? Ask for help from a TA & get it resolved
 * Get help from TA Given an array of integers A, update every element with
 * multiplication of previous and next elements with following exceptions. a)
 * First element is replaced by multiplication of first and second. b) Last
 * element is replaced by multiplication of last and second last.
 * 
 * 
 * Input Format
 * 
 * The only argument given is the integer array A. Output Format
 * 
 * Return the updated array. Constraints
 * 
 * 1 <= length of the array <= 100000 -10^4 <= A[i] <= 10^4 For Example
 * 
 * Input 1: A = [1, 2, 3, 4, 5] Output 1: [2, 3, 8, 15, 20]
 * 
 * Input 2: A = [5, 17, 100, 11] Output 2: [85, 500, 187, 1100]
 */
