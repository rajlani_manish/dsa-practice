package com.intermediate.example.arrays;

import java.util.Arrays;

public class MultipleLeftRotations {

	public static void main(String[] args) {
		int A[] = { 1, 2, 3, 4, 5 };
		int B[] = { 2, 3 };
		System.out.println(Arrays.deepToString(solve(A, B)));
		//solve(A, B);
		
		
	}

	public static int[][] solve(int[] A, int[] B) {
        int[][] output = new int[B.length][A.length];
        int noOfTimesToRotate;

        for(int i=0 ; i<output.length ; i++){

            output[i] = A.clone();
            noOfTimesToRotate=B[i]%A.length;
            output[i] = reverseArray(output[i],0,A.length-1);

            // rotate left sub array
            output[i] = reverseArray(output[i],0, A.length-noOfTimesToRotate-1);

            // rotate right sub array
            output[i] = reverseArray(output[i],A.length-noOfTimesToRotate, A.length-1);
        }
        return output;
    }

    public static int[] reverseArray(int[] A,int start, int end){
        for(;start<end;start++, end--) {
            A[start] = A[start]^A[end];
            A[end] = A[start]^A[end];
            A[start] = A[start]^A[end];
        }
        return A;
    } 
}

//6 5 4 3 2 1
//1 2 3 4 5 6
//k=1
//2 3 4 5 6 1
//k=2
//3 4 5 6 1 2 // needed
//k=3
//4 5 6 1 2 3 // needed

/*
 * Problem Description
 * 
 * Given an array of integers A and multiple values in B which represents number
 * of times array A needs to be left rotated.
 * 
 * Find the rotated array for each value and return the result in the from of a
 * matrix where i'th row represents the rotated array for the i'th value in B.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= length of both arrays <= 2000 -10^9 <= A[i] <= 10^9 0 <= B[i] <= 2000
 * 
 * 
 * Input Format
 * 
 * The first argument given is the integer array A. The second argument given is
 * the integer array B.
 * 
 * 
 * Output Format
 * 
 * Return the resultant matrix.
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [1, 2, 3, 4, 5] B = [2, 3]
 * 
 * Input 2:
 * 
 * 
 * A = [5, 17, 100, 11] B = [1]
 * 
 * 
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * [ [3, 4, 5, 1, 2] [4, 5, 1, 2, 3] ]
 * 
 * 
 * Output 2:
 * 
 * 
 * [ [17, 100, 11, 5] ]
 * 
 * 
 * 
 * Example Explanation
 * 
 * for input 1 -> B[0] = 2 which requires 2 times left rotations
 * 
 * 1: [2, 3, 4, 5, 1]
 * 
 * 2: [3, 4, 5, 1, 2]
 * 
 * B[1] = 3 which requires 3 times left rotation
 * 
 * 1: [2, 3, 4, 5, 1]
 * 
 * 2: [3, 4, 5, 1, 2]
 * 
 * 2: [4, 5, 1, 2, 4]
 */
