package com.intermediate.example.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class RotationGame {

	public static void main(String[] args) {
		
		solve(1);
		/*
		 * Scanner sc = new Scanner(System.in);
		 * //System.out.println("Enter size of array"); int size = sc.nextInt(); int[]
		 * array = new int[size]; //System.out.println("Enter array elements: "); for
		 * (int i = 0; i < size; i++) { array[i] = sc.nextInt(); }
		 * 
		 * //System.out.println("Enter no of right rotations:"); int
		 * noOfTimesRightRotate = sc.nextInt();
		 */

		int[] A = {1, 1, 1, 1, 1 };
		int B = 6;
		int[] rotatedArray = rotateArray(A, B);
		for(int i = 0 ; i < rotatedArray.length; i++)
		{
			System.out.print(rotatedArray[i] + " ");
		}
		//sc.close();
	}
	
	private static int[] rotateArray(int[] A, int noOfTimesRightRotate) {
		int[] output = new int[A.length];
		int noOfTimesToRotate;
		output = A.clone();
		noOfTimesToRotate = noOfTimesRightRotate % A.length;
		output = reverseArray(output, 0, A.length - 1);

		// rotate left sub array
		output = reverseArray(output, 0, noOfTimesToRotate - 1);

		// rotate right sub array
		output = reverseArray(output, noOfTimesToRotate, A.length - 1);
		return output;
	}

	public static int[] reverseArray(int[] A, int start, int end) {
		for (; start < end; start++, end--) {
			A[start] = A[start] ^ A[end];
			A[end] = A[start] ^ A[end];
			A[start] = A[start] ^ A[end];
		}
		return A;
	}
	
	public static void solve(int n){
	    int i = 1;
	    while(i < n){
	        int x = i;
	        while(x!=0){
	            System.out.println("inside x--");
	            x--;
	        }
	        i++;
	    }
	}
   
	
}

/*
 * Q4. Rotation Game
Unsolved
character backgroundcharacter
Stuck somewhere?
Ask for help from a TA & get it resolved
Get help from TA
Problem Description

You are given an integer array A and an integer B. You have to print the same array after rotating it B times towards right.

NOTE: You can use extra memory.



Problem Constraints

1 <= |A| <= 106

1 <= A[i] <= 109

1 <= B <= 109



Input Format

First line begins with an integer |A| denoting the length of array, and then |A| integers denote the array elements.
Second line contains a single integer B



Output Format

Print an array of integers which is the Bth right rotation of input array A, on a separate line.



Example Input

Input 1:

 4 1 2 3 4
 2
Input 2:

 3 1 2 2
 3


Example Output

Output 1:

 3 4 1 2
Output 2:

 1 2 2


Example Explanation

Explanation 1:

 [1,2,3,4] => [4,1,2,3] => [3,4,1,2]

Explanation 2:


 [1,2,2] => [2,1,2] => [2,2,1] => [1,2,2]*/
 