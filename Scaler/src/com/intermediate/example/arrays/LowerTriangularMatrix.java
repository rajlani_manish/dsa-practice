package com.intermediate.example.arrays;

public class LowerTriangularMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] A = { { 1, 0, 0 }, { 0, 1, 0 }, { -7, -8, 9 } };
		System.out.println(solve(A));

	}
	
	public static int solve(final int[][] A) {
		boolean lowerTriMatix = true;
		for(int i =0; i < A.length; i++)
		{
			for(int j = i+1; j<A.length; j++)
			{
				if (A[i][j] != 0) {
					lowerTriMatix = false;
					break;
				}
			}
		}
			return lowerTriMatix ? 1 : 0;
    }

}

/*
 * Q5. Lower Triangular Matrix Unsolved character backgroundcharacter Stuck
 * somewhere? Ask for help from a TA & get it resolved Get help from TA Problem
 * Description
 * 
 * You are given a N X N integer matrix A. You have to tell whether it is a
 * lower triangular matrix or not.
 * 
 * A square matrix is called lower triangular if all the entries above the main
 * diagonal are zero. For any matrix P if elements P[i, j] = 0 (where j > i
 * (1-based)).
 * 
 * image
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 2 <= N <= 103
 * 
 * -1000 <= A[i][j] <= 1000
 * 
 * 
 * 
 * Input Format
 * 
 * First and only argument is a 2D integer matrix A.
 * 
 * 
 * 
 * Output Format
 * 
 * Return 1 if A is a lower triangular matrix, else return 0.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [[1, 0, 0], [0, 0, 0], [-7, -8, 9]] Input 2:
 * 
 * A = [[0, 2], [0, 0]]
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * 1 Output 2:
 * 
 * 0
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * As A[1][2], A[1][3] and A[2][3] are zero, it is a lower triangular matrix.
 * Explanation 2:
 * 
 * As A[1][2] is not equal to zero, it is not a lower triangular matrix.
 * 
 * 
 * See Expected Output Enter your input as per the following guideline There are
 * 1 lines in the input
 * 
 * Line 1 ( Corresponds to arg 1 ) : 2 D array. First 2 integers R, C are the
 * number of rows and columns. Then R * C integers follow corresponding to the
 * rowwise numbers in the 2D array
 */
