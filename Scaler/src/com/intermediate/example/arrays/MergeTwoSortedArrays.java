package com.intermediate.example.arrays;

import java.util.Arrays;

public class MergeTwoSortedArrays {

	public static void main(String[] args) {
		System.out.println(Arrays.toString(solve(new int[] {4, 7, 9}, new int[] {2 ,11, 19})));

	}
	
	public static int[] solve(final int[] A, final int[] B) {
		int[] output = new int[A.length + B.length];
		for(int i = 0; i < A.length; i++ ) {
			output[i] = A[i];
		}
		for(int i = 0; i < B.length; i++ ) {
			output[i + A.length] = B[i];
		}
		Arrays.sort(output);
		return output;
    }

}

/*Merge Two Sorted Arrays
Problem Description

Given two sorted integer arrays A and B, merge B and A as one sorted array and return it as an output.



Problem Constraints

-1010 <= A[i],B[i] <= 1010



Input Format

First Argument is a 1-D array representing A.

Second Argument is also a 1-D array representing B.



Output Format

Return a 1-D vector which you got after merging A and B.



Example Input

Input 1:

A = [4, 7, 9 ]
B = [2 ,11, 19 ]
Input 2:

A = [1]
B = [2]


Example Output

Output 1:

[2, 4, 7, 9, 11, 19]
Output 2:

[1, 2]


Example Explanation

Explanation 1:

Merging A and B produces the output as described above.
Explanation 2:

 Merging A and B produces the output as described above.*/