package com.intermediate.example.arrays;

import java.util.ArrayList;

public class TimeToEquality {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		
		  list.add(731); list.add(349); list.add(490); list.add(781); list.add(271); list.add(405);
		  list.add(811); list.add(181); list.add(102); list.add(126); list.add(866);
		  list.add(16); list.add(622); list.add(492); list.add(194); list.add(735);
		 
		//list.add(2); list.add(4); list.add(1); list.add(3);list.add(2);
		System.out.println(solve(list));
		//731, 349, 490, 781, 271, 405, 811, 181, 102, 126, 866, 16, 622, 492, 194, 735
	}

	public static int solve(ArrayList<Integer> A) {
		int timeRequired = 0;
		int maxElement = Integer.MIN_VALUE;
		for (int i = 0; i < A.size(); i++) {
			if (A.get(i) > maxElement) {
				maxElement = A.get(i);
			}
		}
		System.out.println("max Element :" + maxElement);
		for (int j = 0; j < A.size(); j++) {
			timeRequired += maxElement - A.get(j);
			System.out.print(timeRequired + ", ");
		}
		return timeRequired;
	}

	/*
	 * public int solve(ArrayList<Integer> A) { int timeRequiredInSec = 0; }
	 */

}

/*
 * Q3. Time to equality Unsolved character backgroundcharacter Stuck somewhere?
 * Ask for help from a TA & get it resolved Get help from TA Problem Description
 * 
 * Given an integer array A of size N. In one second you can increase the value
 * of one element by 1.
 * 
 * Find the minimum time in seconds to make all elements of the array equal.
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= N <= 1000000 1 <= A[i] <= 1000
 * 
 * 
 * Input Format
 * 
 * First argument is an integer array A.
 * 
 * 
 * Output Format
 * 
 * Return an integer denoting the minimum time to make all elements equal.
 * 
 * 
 * Example Input
 * 
 * A = [2, 4, 1, 3, 2]
 * 
 * 
 * Example Output
 * 
 * 8
 * 
 * 
 * Example Explanation
 * 
 * We can change the array A = [4, 4, 4, 4, 4]. The time required will be 8
 * seconds.
 * 
 * 
 * See Expected Output Your input 5 8 1 2 3 4 Output 22
 */