package com.intermediate.example.arrays;

public class ConsecutiveElements {

	public static void main(String[] args) {
		int arr[] = { 47, 43, 45, 44, 46 };
		ConsecutiveElements acem = new ConsecutiveElements();
		if (acem.checkArrayContainsConsecutiveElements(arr, arr.length))
			System.out.println("1");
		else
			System.out.println("0");
		return;

	}

	private int getMinimum(int arr[], int n) {
		int min = arr[0];
		for (int i = 1; i < n; i++)
			if (arr[i] < min)
				min = arr[i];
		return min;
	}

	/* Method return maximum value */
	private int getMaximum(int arr[], int n) {
		int max = arr[0];
		for (int i = 1; i < n; i++)
			if (arr[i] > max)
				max = arr[i];
		return max;
	}

	/* This method checks if array elements are consecutive */
	public boolean checkArrayContainsConsecutiveElements(int arr[], int n) {
		if (n < 1)
			return false;

		int min = getMinimum(arr, n);
		int max = getMaximum(arr, n);

		if (max - min + 1 == n) {
			boolean[] visited = new boolean[arr.length];
			for (int i = 0; i < n; i++) {
				if (visited[arr[i] - min] != false)
					return false;

				visited[arr[i] - min] = true;
			}

			return true;
		}
		return false;
	}

}

/*
 * Q2. Array with consecutive elements Solved character backgroundcharacter
 * Stuck somewhere? Ask for help from a TA & get it resolved Get help from TA
 * Problem Description
 * 
 * Given an array of positive integers A, check and return whether the array
 * elements are consecutive or not. NOTE: Try this with O(1) extra space.
 * 
 * 
 * 
 * Problem Constraints
 * 
 * 1 <= length of the array <= 100000 1 <= A[i] <= 10^9
 * 
 * 
 * 
 * Input Format
 * 
 * The only argument given is the integer array A.
 * 
 * 
 * 
 * Output Format
 * 
 * Return 1 if the array elements are consecutive else return 0.
 * 
 * 
 * 
 * Example Input
 * 
 * Input 1:
 * 
 * A = [3, 2, 1, 4, 5] Input 2:
 * 
 * A = [1, 3, 2, 5]
 * 
 * 
 * Example Output
 * 
 * Output 1:
 * 
 * 1 Output 2:
 * 
 * 0
 * 
 * 
 * Example Explanation
 * 
 * Explanation 1:
 * 
 * As you can see all the elements are consecutive, so we return 1. Explanation
 * 2:
 * 
 * Element 4 is missing, so we return 0.
 */
